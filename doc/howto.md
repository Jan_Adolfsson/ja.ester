# Ester (Eraserhead animation editor)

*Ester* is a tool for tuning spritesheet animations and generating json describing those animations. Read the spritesheet and the json into your game and use the json to set up your animations.

This is how *Ester* looks when you start it.

![](Application_1.png)

The user interface is divided in three vertical sections: left, centre and right and each section contains vertically stacked modules. Left section contains the Animation- and Frame-modules, the centre panel contains Player- and Advanced Player-modules and the right contains Spritesheet- and Frame Settings-modules. Each of these modules will be described later on in.



### How to work with Ester

Start a new project by selecting *New* in the *File*-menu. This step can be skipped if the application has just been started, a new project is always created at start up.


#### Setup spritesheet

Load your spritesheet by clicking the *open*-icon in upper right corner of the *Spritesheet*-module. For this example I will be using provided spritesheet called "Example_spritesheet.png". 


###### Sheet size  

Enter the size of the spritesheet, i.e. the size of the spritesheet as a grid of sprites, in the *Sheet size*-fields *Cols* and *Rows*. For "Example_spritesheet.png", these values are: Cols 13 and Rows 5.


###### Sprite size  

Enter the size of the sprites, i.e. the grid cell size, in pixels. For "Example_spritesheet.png" these values are: Width 160 and Height 160. A grid matching the spritesheet should be displayed.

![](Spritesheet_module_1.png)  
*Spritesheet module with spritesheet loaded*



#### Frame settings

In the *Frame settings*-module it's possible to set frame specific values that will act as default values for all frames in all animations. The *Animation*- and *Frames*-modules do provide the option to override these values for both individual animations and frames.


###### Duration  

In the *Duration*-field it's possible to set the default duration time in milliseconds for all frames in all animations.  


###### Frame margins  

The frame margin values are intended to be used for collision and other types of distance detection.


###### Frame offset  

The frame offset values are intended to be used if the spritesheet have sprites not located at the same position inside the grid cells as in other spritesheets. This can be handled with different offset-values in the code, but I find it better to store that information together with the spritesheet description.


![](FrameSettings_module.png)


#### Describe an animation

In a new project an animation called *"New anim"* exists, you can use this as your first animation.  

![](Animations_module_1.png)  


###### Name

Give your animation a name by entering it in the *Name*-field. As an example, let's say you would like to work with the animation found on the fourth row in the "Example_spritesheet.png". Since it's a samurai bowing you could simply call it "Bow", this will be the id of this animation. The name should now appear in the title bar of both *Player*-modules.


###### Index

Setting the animation's start index can be done through both the *Animation*- and the *Spritesheet*-modules.  

Doing it through the *Animation*-module you update the *Index*-fields *Col* and *Row* with values matching where in the spritesheet grid the animation start. For the "Bow"-animation in "Example_spritesheet.png" the values are: Col 0 and Row 3 (the values are zero-based). 

![](Animations_module_1.png)

If the spritesheet is big or you just don't like to count it's probably preferable to set the index through the *Spritesheet*-module. To set the index this way click the *double-headed-arrow*-icon located in the *Spritesheet*-module's title bar to turn on the *Animation range*-tool. The icon should now indicate a pressed state and a blue rectangle should be visible when moving pointer over the grid. 

Left click the cell where the animation should begin, an orange bracket should appear on the left side of the cell and the *Index*-fields in the *Animation*-module should be updated.

![](Spritesheet_module_2.png)
*Spritesheet-module with "Animation range"-tool active and index of one animation set* 


###### Frame count  

Setting the animation's frame count can also be done through both the *Animation*- and the *Spritesheet*-modules.  

Doing it through the *Animation*-module, in the field named *Frames*, enter the number of frames the animation contains. For the "Bow"-animation the frame count is 11.

Going through the *Spritesheet*-module, make sure the *double-headed-arrow*-icon indicates that the *Animation range*-tool is active, then hold down the *Shift*-key and click the last cell of the animation. Orange brackets should now be visible on both the left side of the first cell and on the right side of the last cell of the animation in the grid, and the cells between should be highlighted. 

![](Spritesheet_module_3.png)  
*Spritesheet-module with "Animation range"-tool active and one animation specified* 

The *Frames*-field in the *Animation*-module should have been updated with the correct value and the *Frames*-module should contain as many *Frame X*-fields as number of frames in the animation. 

Updating frame count to any number larger than zero will also trigger the *Player*-modules to render the first frame of the animation. 

![](Application_2.png)  
*Ester with spritesheet and one animation specified*


**Duration**  
If you don't want to use the default frame duration time for the frames in this animation you can override the default value by updating the *Duration*-field with some other value. But if you do want to use the default value this field has be left blank.

It's also possible to override this value, or if this field is blank the default value, and set different duration times for different frames. This is done in the *Frames*-module. Choose the *Frame X*-field for the target frame and enter desired value.

![](Application_3.png)  
*Ester with spritesheet and animation with some individual frame duration times*

**Margin**  

See description for *Frame settings*-module. Enter values to override default, leave blank to use default.  


**Offset**  

See description for *Frame settings*-module. Enter values to override default, leave blank to use default.  


#### Add and remove animations 

Add animation by clicking the *plus*-icon in the header of the *Animations*-module. 

Remove an animation by first selecting it, by clicking on it, and then click the *trashcan*-icon in the header of the *Animations*-module.



#### Order of animations  

The order of the animations can be changed by moving individual animations up or down. Move an animation by first selecting it and then clicking on the *up-arrow*-icon or the *down-arrow*-icon depending on if you want to move it up or down the order.



#### Play animation  



#### Copy to clipboard  

It's possible to copy the content, as json, to the clipboard from following modules:

* Animations  
* Animation  
* Frames  
* Frame settings  

There is a *copy*-icon on the right side of the title bar of these four modules. Click it to copy the content to the clipboard.



#### Save

Save file by clicking *Save*-item in *File*-menu.

The file saved by Ester is a project file containing both gui-settings such as zoom-level as well as the spritesheet descriptions.

To get a file containing only the spritesheet description use the *Export*-function.



#### Export

Export spritesheet description without any workspace information by clicking *Export*-item in *File*-menu.
