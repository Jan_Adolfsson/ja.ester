import React from 'react';
import CollapsablePanel from '../../UI/CollapsablePanel/CollapsablePanel';
import Player from '../../UI/Player/Player';
import PlayerButtonGroup from '../../UI/PlayerButtonGroup/PlayerButtonGroup';

export default class PlayerModule extends React.Component {
    constructor(props){
      super(props);

      this.state = {
        action: "stop",
        repeat: false,
        playButtonIsPressed: false,
        pauseButtonIsPressed: false,
        repeatButtonIsPressed: false
      };

      this.handleOnPlayClick = this.handleOnPlayClick.bind(this);      
      this.handleOnPauseClick = this.handleOnPauseClick.bind(this);      
      this.handleOnStopClick = this.handleOnStopClick.bind(this);            
      this.handleOnZoomChange = this.handleOnZoomChange.bind(this);      
      this.handleOnRepeatClick = this.handleOnRepeatClick.bind(this);            
      this.handleOnAnimationStop = this.handleOnAnimationStop.bind(this);                  
    }
    handleOnRepeatClick = () => {
      this.setState({
        repeat: !this.state.repeat,
        repeatButtonIsPressed: !this.state.repeat
      });
    }
    handleOnZoomChange = (e) => {
      this.props.onZoomChange({ zoom: e.value });
    }
    handleOnPlayClick = () => {
      if(this.state.action === "play") {
        return;
      }

      this.setState({
        action: "play", 
        pauseButtonIsPressed: false,
        playButtonIsPressed: true
      });
    }
    handleOnStopClick = () => {
      if(this.state.action === "stop") {
        return;
      }      

      this.setState({
        action: "stop",
        playButtonIsPressed: false,
        pauseButtonIsPressed: false
      });
    }    
    handleOnPauseClick = () => {
      if(this.state.action === "stop") {
        return;
      } else if(this.state.action === "play") {
        this.setState({
          action: "pause", 
          pauseButtonIsPressed: true
        });
      } else if(this.state.action === "pause")  {
        this.setState({
          action: "play", 
          pauseButtonIsPressed: false
        });
      }
    }    
    handleOnAnimationStop = () => {
      if(this.state.action === "stop") {
        return;
      }      

      this.setState({
        action: "stop", 
        playButtonIsPressed: false
      }); 
    }    
    renderButtons = () => {
      return (
        <PlayerButtonGroup
          zoom={this.props.zoom}
          isPlayButtonPressed={this.state.playButtonIsPressed}
          isPlayButtonDisabled={this.props.animation.frameCount === 0 || !this.props.image}
          isPauseButtonPressed={this.state.pauseButtonIsPressed}
          isRepeatButtonPressed={this.state.repeatButtonIsPressed}
          isRepeatButtonVisible={true}
          onZoomChange={this.handleOnZoomChange}
          onPlayClick={this.handleOnPlayClick}
          onPauseClick={this.handleOnPauseClick}
          onStopClick={this.handleOnStopClick}
          onRepeatClick={this.handleOnRepeatClick}
        />     
      );
    }
    renderPlayer = () => {
      return (
        <Player
          animation={this.props.animation}
          frameCount={this.props.animation ? this.props.animation.frameCount : undefined}
          repeat={this.state.repeat}
          spritesheet={this.props.image}
          spriteSize={this.props.spritesheet.spriteSize}
          gridSize={this.props.spritesheet.gridSize}
          zoom={this.props.zoom}
          action={this.state.action}
          velocity={{x:0, y:0}}
          frameDefaults={this.props.frameDefaults}
          onAnimationStop={this.handleOnAnimationStop}
          style={{height: "450px"}}
        />        
      );
    }
    render() {
      return (
        <div 
          className="module"
        >
          <CollapsablePanel 
            header={this.props.animation ? `${this.props.animation.name}` : "No animation"}
            buttons={this.renderButtons()}
            isCollapsable="true"
          >
            {this.renderPlayer()}
          </CollapsablePanel>
        </div>
      )
    }
  }