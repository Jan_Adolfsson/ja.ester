import React from 'react';
import HeaderButton from '../../../UI/CollapsablePanel/Components/HeaderButton/HeaderButton';

export default class UploadButton extends React.Component {
    handleOnChangeFile = (e) => {
      this.props.onChangeFile({ file:e.target.files[0] });
    }
    handleOnOpenClick = () => {
      this.refs.fileUploader.value = "";
      this.refs.fileUploader.click();
    }

    render() {
      return ( 
        <div>
          <HeaderButton 
            icon="fas fa-folder-open"
            onClick={this.handleOnOpenClick}/>      
          <input 
            type="file" 
            ref="fileUploader"
            style={{display: "none"}}
            onChange={this.handleOnChangeFile}
          />          
        </div>
      )
    }
  }