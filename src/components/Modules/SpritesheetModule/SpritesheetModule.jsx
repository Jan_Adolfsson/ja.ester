import React from 'react';

import CollapsablePanel from '../../UI/CollapsablePanel/CollapsablePanel';
import HeaderGroup from '../../UI/CollapsablePanel/Components/HeaderGroup/HeaderGroup';
import HeaderButton from '../../UI/CollapsablePanel/Components/HeaderButton/HeaderButton';
import NumberPair from '../../UI/NumberPair/NumberPair';
import SpritesheetCanvas from './SpritesheetCanvas/SpritesheetCanvas';
import UploadButton from './UploadButton/UploadButton';
import Zoom from '../../UI/Zoom/Zoom';

import { createCopy, createUpdatedCopy } from '../../../services/copyService';
import { adjustFrames } from '../../../services/frameService';
import { sizeToNumberPair, numberPairToSize } from '../../../services/convertService';
import "./SpritesheetModule.css"

const path = window.require('path');

export default class SpritesheetModule extends React.Component {
    constructor(props){
      super(props);

      this.state = {
        isEditMode: false
      };

      this.handleOnSetInterval = this.handleOnSetInterval.bind(this);  
      this.handleOnChangeFile = this.handleOnChangeFile.bind(this);  
      this.handleOnZoomChange = this.handleOnZoomChange.bind(this);  
      this.handleOnGridSizeChange = this.handleOnGridSizeChange.bind(this);                  
      this.handleOnSpriteSizeChange = this.handleOnSpriteSizeChange.bind(this);                  
    }

    handleOnSetInterval = (e) => {
      this.setState({
        isEditMode: !this.state.isEditMode
      });
    }
    handleOnZoomChange = (e) => {
      this.props.onZoomChange({ zoom: e.value });
    }

    triggerOnSpritesheetChange = (spritesheet) => {
      this.props.onSpritesheetChange({ spritesheet: spritesheet });
    }
    handleOnSpriteSizeChange = (e) => {
      const spritesheet = createUpdatedCopy(this.props.spritesheet, "spriteSize", numberPairToSize(e.pair));
      this.triggerOnSpritesheetChange(spritesheet);
    }
    handleOnGridSizeChange = (e) => {
      const spritesheet = createUpdatedCopy(this.props.spritesheet, "gridSize", numberPairToSize(e.pair));
      this.triggerOnSpritesheetChange(spritesheet);
    }    
    handleOnChangeFile = (e) => {
      this.props.onFileChange({ file: e.file });
    }
    getActiveAnim = () => {
      return this.props.anims[this.props.activeAnimIndex];
    }
    getActiveAnimEndIndex = () => {
      const anim = this.getActiveAnim();
      let 
        frameCount = anim.frameCount,
        endIndex = {
          x: anim.index.x,
          y: anim.index.y
        };

      while(frameCount > 0) {
        const diff = endIndex.x + frameCount - this.props.spritesheet.gridSize.width;
        if(diff > 0) {
          frameCount = diff;
          endIndex = {
            x: 0,
            y: endIndex.y + 1
          };
        } else {
          frameCount = 0;
          endIndex = {
            x: endIndex.x + anim.frameCount,
            y: endIndex.y
          }
        }
      }

      return endIndex;
    }
    frameCountBetween = (start, end) => {
      const 
        lines = (end.y - start.y - 1) * this.props.spritesheet.gridSize.width,
        firstLine = this.props.spritesheet.gridSize.width - start.x,
        lastLine = end.x,
        frameCount = lines + firstLine + lastLine;
        
      return frameCount;
    }
    handleCanvasClick = (index) => {
      const 
        endIndex = this.getActiveAnimEndIndex(),
        frameCount = this.frameCountBetween(index, endIndex),
        anims = createCopy(this.props.anims),
        anim = anims[this.props.activeAnimIndex];
      
      anim.index = index;
      anim.frameCount = frameCount;
      
      adjustFrames(anim.frames, frameCount);        
      
      this.props.onAnimsChange({
        anims: anims
      });
    }
    getActiveAnimStartIndex = () => {
      const anim = this.getActiveAnim();
      return anim.index;
    }
    handleCanvasShiftClick = (index) => {
      const 
        startIndex = this.getActiveAnimStartIndex(),
        frameCount = this.frameCountBetween(startIndex, index) + 1,
        anims = createCopy(this.props.anims),
        anim = anims[this.props.activeAnimIndex];
      
      anim.frameCount = frameCount;

      adjustFrames(anim.frames, frameCount);        

      this.props.onAnimsChange({
        anims: anims
      });
    }
    handleOnCanvasClick = (e) => {
      if(!this.state.isEditMode) {
        return;
      }

      if(e.isShiftDown) {
        this.handleCanvasShiftClick(e.index);
      } else {
        this.handleCanvasClick(e.index);
      }
    }
    formatHeader = () => {
      if(!this.props.image) {
        return "No spritesheet";
      } else
      {
        var filename = path.parse(this.props.imageName).base;
        return `${filename}`;
      }
    }
    renderButtons = () => {
      return (
        <HeaderGroup>    
          <Zoom
            value={this.props.zoom}
            min={0.1}
            max={5}
            step={0.1}
            onValueChange={this.handleOnZoomChange}
          />            
          <HeaderButton 
            icon="fas fa-arrows-alt-h"
            isPressed={this.state.isEditMode}
            onClick={this.handleOnSetInterval}
          />           
          <UploadButton 
            id={this.props.id + ".Button.Open"}
            icon="fas fa-folder-open"
            onChangeFile={this.handleOnChangeFile}
          />           
        </HeaderGroup>
      );
    }
    renderCanvas = () => {
      return (
        <SpritesheetCanvas
          spritesheet={this.props.image}
          gridSize={this.props.spritesheet.gridSize}
          spriteSize={this.props.spritesheet.spriteSize}
          animations={this.props.anims}            
          zoom={this.props.zoom}
          isSelecting={this.state.isEditMode}
          onClick={this.handleOnCanvasClick}
        />
      );
    }
    renderSheetSizeInput = () => {
      return (
        <NumberPair
          heading="Sheet size" 
          label1="Cols"
          label2="Rows"
          inputClassName="number-pair narrow"
          min="0"
          pair={sizeToNumberPair(this.props.spritesheet.gridSize)}
          mandatory={true}
          onPairChange={this.handleOnGridSizeChange}
        />
      );
    }
    renderSpriteSizeInput = () => {
      return (
        <NumberPair
          heading="Sprite size" 
          label1="Width"
          label2="Height"
          inputClassName="number-pair normal"
          min="0"
          pair={sizeToNumberPair(this.props.spritesheet.spriteSize)}
          mandatory={true}                
          onPairChange={this.handleOnSpriteSizeChange}
        />               
      );     
    }
    render() {
      return ( 
        <div 
          className="module"
        >
          <CollapsablePanel 
            header={this.formatHeader()}
            buttons={this.renderButtons()}
            isCollapsable={true}
            isCollapsed={false}
          >
            {this.renderCanvas()}
            <div 
              className="spritesheet-module-settings"
            >  
              {this.renderSheetSizeInput()}          
              {this.renderSpriteSizeInput()}
            </div>              
          </CollapsablePanel>
        </div>
      )
    }
  }