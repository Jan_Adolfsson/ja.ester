import React from 'react';

import "./SpritesheetCanvas.css"

export default class SpritesheetCanvas extends React.Component {
  borderStyle = "rgb(200, 200, 200)";
  gridStyle = "rgb(220, 220, 220)";
  
  constructor(props){
    super(props);

    this.state = {
      mouseIndex: undefined
    }
  }

  componentDidMount() {
    this.updateCanvas();
  }
  componentDidUpdate() {
    this.updateCanvas();
  }

  clearCanvas = () => {
    const ctx = this.refs.canvas.getContext('2d');
    ctx.clearRect(0, 0, this.refs.canvas.width, this.refs.canvas.height);
  }
  drawImage = (image) => {
    const ctx = this.refs.canvas.getContext('2d');
    ctx.drawImage(
      image, 
      0, 
      0, 
      this.refs.canvas.width, 
      this.refs.canvas.height);
  }
  getSpritesheetSize = () => {
    const size = {
      width: this.props.spritesheet.width * this.props.zoom,
      height: this.props.spritesheet.height * this.props.zoom
    }
    return size;
  }
  drawBorder = () => {
    const ctx = this.refs.canvas.getContext('2d');
    ctx.strokeStyle = this.borderStyle;
    ctx.lineWidth = 1;
    ctx.strokeRect(
      0.5, 
      0.5, 
      (this.getSpriteWidth() * this.props.gridSize.width) - 1, 
      (this.getSpriteHeight() * this.props.gridSize.height) - 1);
  }
  getHorizontalLineCount = () => {
    return this.props.gridSize.height - 1;
  }
  getVerticalLineCount = () => {
    return this.props.gridSize.width - 1;
  }  
  getSpriteHeight = () => {
    return this.props.spriteSize.height * this.props.zoom;
  }
  getSpriteWidth = () => {
    return this.props.spriteSize.width * this.props.zoom;
  }
  getPos = (index) => {
    const pos = {
      x: index.x * this.getSpriteWidth(),
      y: index.y * this.getSpriteHeight()
    };
    return pos;
  }
  drawHorizontalLine = (y) => {
    const ctx = this.refs.canvas.getContext('2d');
    ctx.strokeStyle = this.gridStyle;
    ctx.lineWidth = 1;    
    
    ctx.beginPath();
    ctx.moveTo(0, y);
    ctx.lineTo(this.getSpritesheetSize().width, y);
    ctx.stroke();
  }
  drawHorizontalLines = () => {
    const lineCount = this.getHorizontalLineCount();
    const spriteHeight = this.getSpriteHeight();
    for(var i = 1; i < lineCount + 1; ++i) {
      this.drawHorizontalLine(0.5 + i * spriteHeight);
    }
  }
  drawVerticalLine = (x) => {
    const ctx = this.refs.canvas.getContext('2d');
    ctx.strokeStyle = this.gridStyle;
    ctx.lineWidth = 1;    
    
    ctx.beginPath();
    ctx.moveTo(x, 0);
    ctx.lineTo(x, this.getSpritesheetSize().height);
    ctx.stroke();
  }
  drawVerticalLines = () => {
    const lineCount = this.getVerticalLineCount();
    const spriteWidth = this.getSpriteWidth();

    for(var i = 1; i < lineCount + 1; ++i) {
      this.drawVerticalLine(0.5 + i * spriteWidth);
    }    
  }
  shadeCells = (start, frameCount) => {
    const 
      spriteWidth = this.getSpriteWidth(),
      spriteHeight = this.getSpriteHeight(),
      x = start.x * spriteWidth,
      y = start.y * spriteHeight,
      w = frameCount * spriteWidth,
      h = spriteHeight;

    const ctx = this.refs.canvas.getContext('2d');
    ctx.fillStyle = "rgba(255, 255, 255, 0.1)";
    ctx.fillRect(x, y, w, h);
  }
  shadeAnimation = (anim) => {
    let 
      start = anim.index,
      frameCount = anim.frameCount,
      framesToDraw;

    do {
      if(start.x + frameCount > this.props.gridSize.cols) {
        framesToDraw = this.props.gridSize.cols - start.x;
      } else {
        framesToDraw = frameCount;
      }
      this.shadeCells(start, framesToDraw);

      start = {
        x: 0,
        y: start.y + 1
      };

      frameCount -= this.props.gridSize.cols - start.x;        
    } while(framesToDraw > 0)
  }
  shadeAnimations = () => {
    for(var i = 0; i < this.props.animations.length; ++i) {
       this.shadeAnimation(this.props.animations[i]);
    }
  }
  createBeginMarkerPoints = (anim) => {
    const 
      pos = this.getPos(anim.index),
      width = this.getSpriteWidth(),
      height = this.getSpriteHeight();

    const points = [
      {
        x: pos.x + 2 + (0.1 * width),
        y: pos.y
      },
      {
        x: pos.x + 2,
        y: pos.y
      },
      {
        x: pos.x + 2,
        y: pos.y + height
      },
      {
        x: pos.x + 2 + (0.1 * width),
        y: pos.y + height
      }
    ];
    return points;
  }
  drawAnimationBeginMarker = (anim) => {
    const points = this.createBeginMarkerPoints(anim);
    
    const ctx = this.refs.canvas.getContext('2d');    
    ctx.strokeStyle = "rgb(255, 125, 0)";
    ctx.lineWidth= 2;
    
    ctx.beginPath();
    ctx.moveTo(points[0].x, points[0].y);
    ctx.lineTo(points[1].x, points[1].y);
    ctx.lineTo(points[2].x, points[2].y);
    ctx.lineTo(points[3].x, points[3].y);
    ctx.stroke();
  }
  drawAnimationBeginMarkers = () => {
    for(var i = 0; i < this.props.animations.length; ++i) {
      this.drawAnimationBeginMarker(this.props.animations[i]);
    }
  }
  createEndMarkerPoints = (anim) => {
    const index = {
      x: anim.index.x,
      y: anim.index.y
    };
      
    index.x = anim.index.x + anim.frameCount;
    let frameCount = index.x - this.props.gridSize.cols;
    while(index.x > this.props.gridSize.cols) {
      ++index.y;
      index.x = frameCount;
      frameCount = index.x - this.props.gridSize.cols;
    }

    const pos = this.getPos(index);
    const points = [
      {
        x: pos.x - 1 - (0.1 * this.getSpriteWidth()),
        y: pos.y
      },
      {
        x: pos.x - 1,
        y: pos.y
      },
      {
        x: pos.x - 1,
        y: pos.y + this.getSpriteHeight()
      },
      {
        x: pos.x - 1 - (0.1 * this.getSpriteWidth()),
        y: pos.y + this.getSpriteHeight()
      }
    ];
    return points;
  }
  drawAnimationEndMarker = (anim) => {
    const points = this.createEndMarkerPoints(anim);
    
    const ctx = this.refs.canvas.getContext('2d');    
    ctx.strokeStyle = "rgb(255, 125, 0)";
    ctx.lineWidth= 2;
    
    ctx.beginPath();
    ctx.moveTo(points[0].x, points[0].y);
    ctx.lineTo(points[1].x, points[1].y);
    ctx.lineTo(points[2].x, points[2].y);
    ctx.lineTo(points[3].x, points[3].y);
    ctx.stroke();    
  }
  drawAnimationEndMarkers = () => {
    for(var i = 0; i < this.props.animations.length; ++i) {
      this.drawAnimationEndMarker(this.props.animations[i]);
    }    
  }  
  drawCellFocusRect = () => {
    const 
      x = this.mouseIndex.x * this.getSpriteWidth(),
      y = this.mouseIndex.y * this.getSpriteHeight(),
      w = this.getSpriteWidth(),
      h = this.getSpriteHeight();

    const ctx = this.refs.canvas.getContext('2d');    
    ctx.strokeStyle = "rgb(0, 0, 255)";
    
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x + w, y);
    ctx.lineTo(x + w, y + h);
    ctx.lineTo(x, y + h);
    ctx.closePath();
    ctx.stroke();
  }
  drawGrid = () => {
    this.shadeAnimations();
    this.drawAnimationBeginMarkers();
    this.drawAnimationEndMarkers();
    this.drawBorder();
    this.drawHorizontalLines();
    this.drawVerticalLines();
  }
  updateCanvas() {
    if(!this.props.spritesheet) {
      this.clearCanvas();
      return;
    }

    this.drawImage(this.props.spritesheet);

    if(!this.props.animations) {
      return;
    }
    this.drawGrid(); 

    if(this.props.isSelecting) {
      this.drawCellFocusRect();
    }    
  }
  getMousePos = (evt) => {
    var rect = this.refs.canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
  }  
  getIndexFromCoordinates = (pos) => {
    const index = {
      x: Math.floor(pos.x / this.getSpriteWidth()),
      y: Math.floor(pos.y / this.getSpriteHeight())
    };
    return index;
  }
  handleOnMouseMove = (e) => {
    if(!this.props.spritesheet) {
      return;
    }
    
    const pos = this.getMousePos(e);
    this.mouseIndex = this.getIndexFromCoordinates(pos);
    this.setState({mouseIndex: this.mouseIndex});
  }
  handleOnMouseLeave = (e) => {
    if(!this.props.spritesheet) {
      return;
    }

    this.setState({mouseIndex: undefined});
  }
  handleOnClick = (e) => {
    if(!this.props.spritesheet) {
      return;
    }

    const pos = this.getMousePos(e);
    this.mouseIndex = this.getIndexFromCoordinates(pos);
    this.props.onClick({
      index: this.mouseIndex,
      isShiftDown: e.shiftKey
    });    
  }
  render() {
    if(this.props.spritesheet && this.refs.canvas) {
      this.refs.canvas.width = this.props.spritesheet.width * this.props.zoom;
      this.refs.canvas.height = this.props.spritesheet.height * this.props.zoom;
  }

    return (
      <div className="spritesheet-canvas">
        <canvas 
          ref="canvas"
          className="spritesheet-canvas"
          onMouseMove={this.handleOnMouseMove}
          onMouseLeave={this.handleOnMouseLeave}
          onClick={this.handleOnClick}
        />
      </div>        
    );
  }
}