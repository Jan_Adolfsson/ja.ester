import React from 'react';
import CollapsablePanel from '../../../UI/CollapsablePanel/CollapsablePanel';
import LabelInput from '../../../UI/LabelInput/LabelInput';
import HeaderGroup from '../../../UI/CollapsablePanel/Components/HeaderGroup/HeaderGroup';
import HeaderButton from '../../../UI/CollapsablePanel/Components/HeaderButton/HeaderButton';
import NumberPair from '../../../UI/NumberPair/NumberPair';
import NumberQuadrupel from '../../../UI/NumberQuadrupel/NumberQuadrupel';

import { adjustFrames } from '../../../../services/frameService';
import { createUpdatedCopy } from '../../../../services/copyService';
import { copyAsJsonToClipboard } from '../../../../services/clipboardService';
import { 
  numberPairToPoint, 
  pointToNumberPair,
  numberQuadrupelToMargin, 
  marginToNumberQuadrupel 
} from '../../../../services/convertService';
import { getInteger } from '../../../../services/integerService';

import "./Animation.css"

export default class Animation extends React.Component {
  constructor(props){
      super(props);

      this.state = {
        hasFocus: false
      };

      this.handleOnNameChange = this.handleOnNameChange.bind(this);        
      this.handleOnFrameOffsetChange = this.handleOnFrameOffsetChange.bind(this);  
      this.handleOnClipboardClick = this.handleOnClipboardClick.bind(this);    
      this.handleOnFrameMarginChange = this.handleOnFrameMarginChange.bind(this);  

      this.handleOnIndexChange = this.handleOnIndexChange.bind(this);  
      this.handleOnFrameCountChange = this.handleOnFrameCountChange.bind(this);        
      this.handleOnFrameDurationChange = this.handleOnFrameDurationChange.bind(this);        
    }
    handleOnClipboardClick = () => {
      copyAsJsonToClipboard(this.props.anim, "animation");
    }
    handleOnResetClick = () => {
      this.props.onResetClick();
    }  
    handleOnClick = (e) => {
      if(this.state.hasFocus) {
        return;
      }
      this.setState({ 
          hasFocus: true
        },
        () => {
          this.handleOnFocus();
        });
    }      
    handleOnFocus = (e) => {
      this.props.onFocus(this);
    }
    handleOnBlur = (e) => {
      this.setState({
        hasFocus: false
      });
    }
    isValidName = (name) => {
      return this.props.anims.find((n) => n === name);
    }

    copyAnimAndTriggerOnAnimChange = (propName, propValue) => {
      const anim = createUpdatedCopy(this.props.anim, propName, propValue);
      this.props.onAnimChange({ anim: anim });      
    }
    handleOnNameChange = (e) => {
      this.copyAnimAndTriggerOnAnimChange("name", e.value);
    }
    handleOnIndexChange = (e) => {
      this.copyAnimAndTriggerOnAnimChange("index", numberPairToPoint(e.pair));
    }
    handleOnFrameCountChange = (e) => {
      const anim = createUpdatedCopy(this.props.anim, "frameCount", parseInt(e.value, 10));
      adjustFrames(anim.frames, e.value);
      this.props.onAnimChange({ anim: anim });      
    }
    handleOnFrameDurationChange = (e) => {
      this.copyAnimAndTriggerOnAnimChange("frameDuration", getInteger(e.value));
    }            
    handleOnFrameOffsetChange = (e) => {
      this.copyAnimAndTriggerOnAnimChange("frameOffset", numberPairToPoint(e.pair));
    }        
    handleOnFrameMarginChange = (e) => {
      this.copyAnimAndTriggerOnAnimChange("frameMargin", numberQuadrupelToMargin(e.quadrupel));      
    }  
    
    renderButtons = () => {
      return (
        <HeaderGroup>  
          <HeaderButton 
            icon="fas fa-copy"
            onClick={this.handleOnClipboardClick}/>                       
        </HeaderGroup>
      );
    }
    renderNameInput = () => {
      return (
        <LabelInput 
          label="Name" 
          labelClassName="no-left-margin"
          inputClassName="extra-wide"
          value={this.props.anim.name}
          invalidValues={this.props.anims.map((a) => a.name)}
          onValueChange={this.handleOnNameChange}            
        />
      );
    }
    renderIndexInput = () => {
      return (
        <NumberPair
          heading="Index" 
          label1="Col"
          label2="Row"
          inputClassName="number-pair narrow"
          pair={pointToNumberPair(this.props.anim.index)}
          min={0}
          mandatory={true}
          onPairChange={this.handleOnIndexChange}
        /> 
      );
    }
    renderFramesInput = () => {
      return (
        <LabelInput 
          label="Frames" 
          labelClassName="no-left-margin"            
          inputClassName="narrow"
          type="number"
          value={this.props.anim.frameCount}
          min={0}
          mandatory={true}
          onValueChange={this.handleOnFrameCountChange}
        />
      );
    }
    renderDurationInput = () => {
      return (
        <LabelInput 
          label="Duration" 
          labelClassName="no-left-margin"            
          inputClassName="normal"
          step="50"
          type="number"
          value={this.props.anim.frameDuration}
          min={0}
          mandatory={false}
          onValueChange={this.handleOnFrameDurationChange}
        /> 
      );
    }
    renderMarginsInput = () => {
      return (
        <NumberQuadrupel 
          heading="Frame margins" 
          inputClassName="numberquadrupel normal"
          label1="Top"
          label2="Right"
          label3="Bottom"
          label4="Left"
          quadrupel={marginToNumberQuadrupel(this.props.anim.frameMargin)}
          min={0}
          mandatory={false}
          onQuadrupelChange={this.handleOnFrameMarginChange}
        />       
      );
    }
    renderOffsetInput = () => {
      return (
        <NumberPair
          heading="Frame offset" 
          inputClassName="number-pair normal"
          label1="X"
          label2="Y"
          pair={pointToNumberPair(this.props.anim.frameOffset)}
          onPairChange={this.handleOnFrameOffsetChange}
        />
      );
    }
    renderResetButton = () => {
      return (
        <button 
          className="Modulebutton"
          onClick={this.handleOnResetClick}
        >
          Reset
        </button>    
      );
    }                    
    render() {
      return (
        <div 
          className="animation"
          onClick={this.handleOnClick}
          onFocus={this.handleOnFocus}
          onBlur={this.handleOnBlur}
        >
          <CollapsablePanel 
            header={this.props.anim.name}
            buttons={this.renderButtons()}
            isCollapsable
            isActive={this.props.isActive}
          >
            {this.renderNameInput()}
            {this.renderIndexInput()}
            {this.renderFramesInput()}
            {this.renderDurationInput()}
            {this.renderMarginsInput()}
            {this.renderOffsetInput()}
            {this.renderResetButton()}        
          </CollapsablePanel>
        </div>
      );
    }
  }