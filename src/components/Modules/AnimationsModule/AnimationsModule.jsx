import React from 'react';
import CollapsablePanel from '../../UI/CollapsablePanel/CollapsablePanel';
import HeaderGroup from '../../UI/CollapsablePanel/Components/HeaderGroup/HeaderGroup';
import HeaderButton from '../../UI/CollapsablePanel/Components/HeaderButton/HeaderButton';
import Animation from './Animation/Animation';

import { createCopy } from '../../../services/copyService';
import { copyAsJsonToClipboard } from '../../../services/clipboardService';
import "./AnimationsModule.css"

export default class AnimationsModule extends React.Component {
  constructor(props){
      super(props);

      this.state = {
        activeIndex: this.props.activeIndex
      }

      this.handleOnAddClick = this.handleOnAddClick.bind(this);  
      this.handleOnClipboardClick = this.handleOnClipboardClick.bind(this);    
    }
    static getDerivedStateFromProps = (nextProps, prevState) => {
      return {
        value: nextProps.value
      };
    }    

    triggerOnAnimsChange = (anims, index) => {
      this.props.onAnimsChange({ 
          anims: anims, 
          activeIndex: index 
        }
      );
    }
    getHighestId = () => {
      let highest = -1;
      for(let i = 0; i < this.props.anims.length; ++i) {
        if(this.props.anims[i].id > highest) {
          highest = this.props.anims[i].id;
        }
      }
      return highest;      
    }
    newId = () => {
      return this.getHighestId() + 1;
    }
    createNewAnimation = () => {
      const anim = {
        frameCount: 0,
        frameDuration: this.props.defaults.frameDuration,
        frameMargin: this.props.defaults.frameMargin,
        frameOffset: this.props.defaults.frameOffset,
        frames: [],
        id: this.newId(),
        index: {
          x: 0,
          y: 0
        },
        loop: true,
        name: `New anim`
      }
      return anim;
    }  
    handleOnAddClick = () => {
      const anims = createCopy(this.props.anims);
      const anim = this.createNewAnimation();
      anims.push(anim);    
  
      this.triggerOnAnimsChange(anims);
    }    
    changeActiveIndex = (index) => {
      index = 
        index < this.props.anims.length - 1
          ? index
          : this.props.anims.length - 1;
      
      const eventArg = {
        oldIndex: this.state.activeIndex,
        newIndex: index
      };

      this.setState({
          activeIndex: index
        },
        () => {
          this.props.onActiveAnimationChange(eventArg);          
        }
      );
    }
    handleOnAnimClick = (e) => {
      this.changeActiveIndex(e.props.index);
    } 
    handleOnFocus = (e) => {
      this.changeActiveIndex(e.props.index);
    }
    handleOnClipboardClick = () => {
      copyAsJsonToClipboard(this.props.anims, "animations");
    }
    handleOnResetClick = () => {
      const anims = createCopy(this.props.anims);
      const anim = anims[this.state.activeIndex];
      anim.frameDuration = this.props.defaults.frameDuration;
      anim.frameMargin = this.props.defaults.frameMargin;
      anim.frameOffset = this.props.defaults.frameOffset;

      this.triggerOnAnimsChange(anims);
    }
    handleOnDeleteClick = () => {
      const activeIndex = 
        this.state.activeIndex === this.props.anims.length - 1
          ? this.state.activeIndex - 1
          : this.state.activeIndex;
      
      const anims = createCopy(this.props.anims);
      anims.splice(this.state.activeIndex, 1);

      this.setState({ 
          activeIndex: activeIndex
        },
        () => {
          this.triggerOnAnimsChange(anims, activeIndex);
        }
      ); 
    }
    handleOnMoveDownClick = () => {
      const 
        anims = createCopy(this.props.anims),
        anim = anims.splice(this.state.activeIndex, 1)[0],
        index = this.state.activeIndex + 1;
      
      anims.splice(index, 0, anim);

      this.setState({
        activeIndex: index
      },
      () => {
        this.triggerOnAnimsChange(anims, index);
      })
    }
    handleOnMoveUpClick = () => {
      const 
        anims = createCopy(this.props.anims),
        anim = anims.splice(this.state.activeIndex, 1)[0],
        index = this.state.activeIndex - 1;
      
      anims.splice(index, 0, anim);

      this.setState({
        activeIndex: index
      },
      () => {
        this.triggerOnAnimsChange(anims, index);
      })      
    }        
    handleOnAnimChange = (e) => {
      const anims = createCopy(this.props.anims);
      anims[this.state.activeIndex] = e.anim;
      
      this.triggerOnAnimsChange(anims);
    } 

    renderAnimation = (anim, index) => {
      return (
        <Animation  
          key={anim.id}
          anim={anim}
          index={index}
          isActive={this.state.activeIndex === index}
          anims={this.props.anims}
          onFocus={this.handleOnFocus}
          onAnimChange={this.handleOnAnimChange}
          onResetClick={this.handleOnResetClick}
         />
      );
    }
    renderAnimations() {
      const animations = 
        this
          .props
          .anims
          .map((anim,index) => this.renderAnimation(anim, index));

      return animations;
    }
    renderButtons = () => {
      console.log(this.props.activeIndex);

      return (
        <HeaderGroup>     
          <HeaderButton 
            icon="fas fa-copy"
            onClick={this.handleOnClipboardClick}/>           
          <HeaderButton 
            id="Button.Plus"
            icon="fas fa-plus"
            onClick={this.handleOnAddClick}/>
          <HeaderButton 
            icon="fas fa-trash"
            isDisabled={this.props.anims.length <= 1}
            onClick={this.handleOnDeleteClick}/>
          <HeaderButton 
            icon="fas fa-arrow-up"
            isDisabled={this.props.activeIndex === 0}
            onClick={this.handleOnMoveUpClick}/>
          <HeaderButton 
            icon="fas fa-arrow-down"
            isDisabled={this.props.activeIndex === this.props.anims.length - 1}
            onClick={this.handleOnMoveDownClick}/>
          </HeaderGroup>
      );      
    }
    render() {
      return (
        <div 
          className="module animations-module"
        >
          <CollapsablePanel 
            header="Animations"
            buttons={this.renderButtons()}
            isCollapsable={this.props.isCollapsable}
          >
            {this.renderAnimations()}
          </CollapsablePanel>    
        </div>
      );
    }
  }