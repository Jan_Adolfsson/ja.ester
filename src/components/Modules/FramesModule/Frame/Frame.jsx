import React from 'react';
import LabelInput from '../../../UI/LabelInput/LabelInput';

import { getInteger } from '../../../../services/integerService';
import "./Frame.css"

export default class Frame extends React.Component {
    constructor(props){
        super(props);

        this.handleOnValueChange = this.handleOnValueChange.bind(this);            
    }
    handleOnValueChange = (e) => {
      this.props.onFrameChange({ 
          frame: {
            index: this.props.frame.index,
            duration: getInteger(e.value)            
          }
        }
      );
    }
    render() {
      return (
        <LabelInput 
          label={this.props.label}
          inputClassName="normal"
          type="number"
          step="50"
          value={this.props.frame.duration === undefined ? "" : this.props.frame.duration}
          min={0}
          onValueChange={this.handleOnValueChange}
        />
      );
    }
  }