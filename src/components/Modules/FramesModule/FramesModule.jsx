import React from 'react';
import CollapsablePanel from '../../UI/CollapsablePanel/CollapsablePanel';
import Frame from './Frame/Frame';
import HeaderGroup from '../../UI/CollapsablePanel/Components/HeaderGroup/HeaderGroup';
import HeaderButton from '../../UI/CollapsablePanel/Components/HeaderButton/HeaderButton';

import { createUpdatedCopy } from '../../../services/copyService';
import { copyAsJsonToClipboard } from '../../../services/clipboardService';

import "./FramesModule.css"

export default class FramesModule extends React.Component {
  constructor(props){
      super(props);

      this.handleOnFrameChange = this.handleOnFrameChange.bind(this);        
      this.handleOnClipboardClick = this.handleOnClipboardClick.bind(this);    
    }
    handleOnClipboardClick = () => {
      copyAsJsonToClipboard(this.props.frames, "frames");
    }  
    handleOnFrameChange = (e) => {
      const frames = createUpdatedCopy(this.props.frames, e.frame.index, e.frame);
      this.props.onFramesChange({ frames: frames });
    }
    handleOnResetClick = () => {
      this.props.onResetClick();
    }    
    renderButtons = () => {
      return (
        <HeaderGroup>    
          <HeaderButton 
            icon="fas fa-copy"
            onClick={this.handleOnClipboardClick}/>           
        </HeaderGroup>
      );      
    }
    canRenderFrames = () => {
      return this.props.frames && this.props.frames.length > 0;
    }
    renderFrame = frame => {
      return (
        <Frame
          key={frame.index}
          label={"Frame " + frame.index}
          frame={frame}
          onFrameChange={this.handleOnFrameChange}
        />
      );
    }
    renderFrames = () => {
      const frames = 
        this
          .props
          .frames
          .map(frame => this.renderFrame(frame));
          
      return frames;
    }
    renderResetButton = () => {
      return (
        <button 
          className="Modulebutton"
          onClick={this.handleOnResetClick}
        >
          Reset
        </button>            
      );     
    }
    render() {
      return (
        <div 
          className="module"
        >        
          <CollapsablePanel 
            header="Frames"
            isCollapsable={true}
            buttons={this.renderButtons()}
          >
            {
              this.canRenderFrames() ? 
                this.renderFrames() :
                (null)
            }
            {this.renderResetButton()}
          </CollapsablePanel>
        </div>     
      );
    }
  }