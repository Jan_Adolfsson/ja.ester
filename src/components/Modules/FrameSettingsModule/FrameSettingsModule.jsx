import React from 'react';
import CollapsablePanel from '../../UI/CollapsablePanel/CollapsablePanel';
import LabelInput from '../../UI/LabelInput/LabelInput';
import NumberQuadrupel from '../../UI/NumberQuadrupel/NumberQuadrupel';
import NumberPair from '../../UI/NumberPair/NumberPair';
import HeaderGroup from '../../UI/CollapsablePanel/Components/HeaderGroup/HeaderGroup';
import HeaderButton from '../../UI/CollapsablePanel/Components/HeaderButton/HeaderButton';

import { createUpdatedCopy } from '../../../services/copyService';
import 
  { 
    pointToNumberPair, 
    numberPairToPoint, 
    marginToNumberQuadrupel, 
    numberQuadrupelToMargin 
  } 
  from '../../../services/convertService';
import { copyAsJsonToClipboard } from '../../../services/clipboardService';
import "./FrameSettingsModule.css"

export default class FrameSettingsModule extends React.Component {
    triggerOnSettingsChange = (settings) => {
      this.props.onSettingsChange({ settings: settings });
    }
    handleOnOffsetChange = (e) => {
      const settings = createUpdatedCopy(this.props.settings, "offset", numberPairToPoint(e.pair));
      this.triggerOnSettingsChange(settings);
    }        
    handleOnMarginChange = (e) => {
      const quadrupel = numberQuadrupelToMargin(e.quadrupel);
      const settings = createUpdatedCopy(this.props.settings, "margin", quadrupel);
      this.triggerOnSettingsChange(settings);
    }        
    handleOnDurationChange = (e) => {
      const settings = createUpdatedCopy(this.props.settings, "duration", parseInt(e.value, 10));
      this.triggerOnSettingsChange(settings);
    }    
    handleOnClipboardClick = () => {
      copyAsJsonToClipboard(this.props.settings, "settings");
    }        
    handleOnResetClick = () => {
      this.props.onResetClick();
    }
    renderButtons = () => {
      return (
        <HeaderGroup>    
          <HeaderButton 
            icon="fas fa-copy"
            onClick={this.handleOnClipboardClick}/>           
        </HeaderGroup>
      );
    }
    renderDurationInput = () => {
      return (
        <LabelInput 
          label="Duration"
          type="number" 
          step="50"
          labelClassName="no-left-margin"
          inputClassName="normal"
          value={this.props.settings.duration}
          min={0}
          mandatory={true}
          onValueChange={this.handleOnDurationChange}
        />                      
      );
    }
    renderMarginInput = () => {
      return (
        <NumberQuadrupel 
          heading="Frame margins" 
          inputClassName="numberquadrupel normal"
          label1="Top"
          label2="Right"
          label3="Bottom"
          label4="Left"
          quadrupel={marginToNumberQuadrupel(this.props.settings.margin)}
          mandatory={true}     
          min={0}                       
          onQuadrupelChange={this.handleOnMarginChange}
        />
      );
    }
    renderOffsetInput = () => {
      return (
        <NumberPair
          heading="Frame offset" 
          inputClassName="number-pair normal"
          label1="X"
          label2="Y"
          pair={pointToNumberPair(this.props.settings.offset)}
          mandatory={true}              
          onPairChange={this.handleOnOffsetChange}
        />    
      );
    }
    renderResetButton = () => {
      return (
        <button 
          className="Modulebutton"
          onClick={this.handleOnResetClick}
        >
          Reset
        </button>
      );     
    }
    render() {
      return ( 
        <div 
          className="module"
        >
          <CollapsablePanel 
            header="Frame settings"
            isCollapsable="true"
            buttons={this.renderButtons()}
          >
            {this.renderDurationInput()}
            {this.renderMarginInput()}
            {this.renderOffsetInput()}
            {this.renderResetButton()}
          </CollapsablePanel>
        </div>
        )
    }
  }