import React from 'react';
import CollapsablePanel from '../../UI/CollapsablePanel/CollapsablePanel';
import Player from '../../UI/Player/Player';
import NumberPair from '../../UI/NumberPair/NumberPair';
import PlayerButtonGroup from '../../UI/PlayerButtonGroup/PlayerButtonGroup';
import { numberPairToPoint, pointToNumberPair } from '../../../services/convertService';


export default class AdvancedPlayerModule extends React.Component {
    constructor(props){
      super(props);

      this.state = {
        action: "stop",
        playButtonIsPressed: false,
        pauseButtonIsPressed: false,
        repeatButtonIsPressed: false
      };

      this.handleOnPlayClick = this.handleOnPlayClick.bind(this);      
      this.handleOnPauseClick = this.handleOnPauseClick.bind(this);      
      this.handleOnStopClick = this.handleOnStopClick.bind(this);            
      this.handleOnZoomChange = this.handleOnZoomChange.bind(this);      
      this.handleOnAnimationStop = this.handleOnAnimationStop.bind(this);                  
    }
    handleOnPlayClick = () => {
      if(this.state.action === "play") {
        return;
      }

      this.setState({
        action: "play", 
        pauseButtonIsPressed: false,
        playButtonIsPressed: true
      });
    }
    handleOnStopClick = () => {
      if(this.state.action === "stop") {
        return;
      }      

      this.setState({
        action: "stop",
        playButtonIsPressed: false,
        pauseButtonIsPressed: false
      });
    }    
    handleOnPauseClick = () => {
      if(this.state.action === "stop") {
        return;
      } else if(this.state.action === "play") {
        this.setState({
          action: "pause", 
          pauseButtonIsPressed: true
        });
      } else if(this.state.action === "pause")  {
        this.setState({
          action: "play", 
          pauseButtonIsPressed: false
        });
      }
    }    
    handleOnAnimationStop = () => {
      if(this.state.action === "stop") {
        return;
      }      

      this.setState({
        action: "stop", 
        playButtonIsPressed: false
      }); 
    }  
    handleOnZoomChange = (e) => {
      this.props.onZoomChange({zoom: e.value});
    }
    handleOnVelocityChange = (e) => {
      this.props.onVelocityChange({ velocity: numberPairToPoint(e.pair) });
    }  
    handleOnPositionChange = (e) => {
      this.props.onPositionChange({ position: numberPairToPoint(e.pair) });
    }  
    handleOnResetClick = () => {
      this.props.onResetClick()
    }    
    renderButtons = () => {
      return (
        <PlayerButtonGroup
          zoom={this.props.zoom}
          isPlayButtonPressed={this.state.playButtonIsPressed}
          isPlayButtonDisabled={this.props.animation.frameCount === 0 || !this.props.image}
          isPauseButtonPressed={this.state.pauseButtonIsPressed}
          isRepeatButtonVisible={false}
          onZoomChange={this.handleOnZoomChange}
          onPlayClick={this.handleOnPlayClick}
          onPauseClick={this.handleOnPauseClick}
          onStopClick={this.handleOnStopClick}
        />     
      );
    }    
    renderPlayer = () => {
      return (
        <Player
          style={{height: "450px"}}
          animation={this.props.animation}
          frameCount={this.props.animation ? this.props.animation.frameCount : undefined}
          position={this.props.position}
          repeat={true}
          spritesheet={this.props.image}
          spriteSize={this.props.spritesheet.spriteSize}
          gridSize={this.props.spritesheet.gridSize}
          zoom={this.props.zoom}
          action={this.state.action}
          velocity={this.props.velocity}
          frameDefaults={this.props.frameDefaults}          
          onAnimationStop={this.handleOnAnimationStop}
        />        
      );
    }
    renderVelocityInput = () => {
      return (
        <NumberPair
          heading="Velocity"
          label1="X"
          label2="Y"
          inputClassName="number-pair narrow"
          pair={pointToNumberPair(this.props.velocity)}
          mandatory={true}
          onPairChange={this.handleOnVelocityChange}
        />
      );
    }
    renderPositionInput = () => {
      return (
        <NumberPair
          heading="Position"
          label1="X"
          label2="Y"
          inputClassName="number-pair narrow"
          pair={pointToNumberPair(this.props.position)}
          mandatory={true}              
          onPairChange={this.handleOnPositionChange}
        />
      );
    }
    renderResetButton = () => {
      return (
        <button 
          className="Modulebutton"
          onClick={this.handleOnResetClick}
        >
          Reset
        </button>        
      );
    }
    render() {
      return (
        <div 
          className="module"
        >
          <CollapsablePanel 
            header={this.props.animation ? `${this.props.animation.name}` : "No animation"}
            buttons={this.renderButtons()}
            isCollapsable="true"
          >

            {!this.isCollapsed && this.renderPlayer()}
            <div
              style={{display: "flex"}}
            >
              {!this.isCollapsed && this.renderVelocityInput()}
              {!this.isCollapsed && this.renderPositionInput()}
            </div>
            {!this.isCollapsed && this.renderResetButton()}
          </CollapsablePanel>
        </div>
      )
    }
  }