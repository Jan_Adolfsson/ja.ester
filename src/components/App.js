import React, { Component } from 'react';
import AnimationsModule from './Modules/AnimationsModule/AnimationsModule';
import FramesModule from './Modules/FramesModule/FramesModule';
import PlayerModule from './Modules/PlayerModule/PlayerModule';
import AdvancedPlayerModule from './Modules/AdvancedPlayerModule/AdvancedPlayerModule';
import SpritesheetModule from './Modules/SpritesheetModule/SpritesheetModule';
import FrameSettingsModule from './Modules/FrameSettingsModule/FrameSettingsModule';

import '../styles/App.css';
import '../styles/Panel.css';
import '../styles/Module.css';
import '../fonts/fontAwesome/webfonts/fontawesome-all.min.css';
import { 
  createEmptyProject,
  createProjectFromState, 
  loadRecentProject, 
  createFrameDefaults,
  createDefaultVelocity,
  createDefaultPosition, 
  saveProject,
  getRecentProjectPath
} from '../services/projectService';
import { loadSpritesheet } from '../services/spritesheetService'
import { createCopy} from '../services/copyService';
import { MainMenu } from '../services/menuService';
import { 
  showSaveDlg,
  showSaveQuestion, 
  showErrorDlg 
} from '../services/dialogService';

const path = window.require('path');
const {remote} = window.require('electron');
const {ipcRenderer} = window.require('electron');
const {Menu} = remote;

class App extends Component {
  loadRecent = false;

  createInitialState = (proj) => {
    return {
      path: undefined,
      image: undefined,
      frameDefaults: proj.frameDefaults,
      spritesheet: proj.spritesheet,
      animations: proj.animations,
      activeAnimationIndex: proj.modules.animations.activeIndex,
      isDirty: false,
      advancedPlayerZoom: proj.modules.advancedPlayer.zoom,
      advancedPlayerPosition: proj.modules.advancedPlayer.position,
      advancedPlayerVelocity: proj.modules.advancedPlayer.velocity,
      playerZoom: proj.modules.player.zoom,
      spritesheetZoom: proj.modules.spritesheet.zoom
    };
  }
  constructor(props){
    super(props);
    
    const proj = createEmptyProject();
    this.state = this.createInitialState(proj);

    ipcRenderer.on('app-close', (e) => {
      if(!this.isProjDirty()) {
        ipcRenderer.send('closed');
        return;
      }

      const saveReply = showSaveQuestion();      
      if(saveReply === 'cancel') {
        return;
      } 
      
      if(saveReply === 'dontsave') {
        ipcRenderer.send('closed');
        return;
      } 
      
      const fileReply = showSaveDlg();
      if(!fileReply) {
        return;
      }

      const proj = createProjectFromState(this.state);
      saveProject(
        proj, 
        fileReply, 
        (err) => { 
          if(err) { 
            showErrorDlg("Error saving file.");
            return;
          } else {
            ipcRenderer.send('closed');
          }
        }        
      );
    });
  }
  isProjDirty = () => {
    return this.state.isDirty;
  }
  isProjNew = () => {
    return !this.state.path;
  }
  getProj = () => {
    return createProjectFromState(this.state);
  }
  getProjPath = () => this.state.path;
  getSpritesheetDesc = () => {
    const desc = {
      spritesheet: createCopy(this.state.spritesheet),
      frameDefaults: createCopy(this.state.frameDefaults),
      animations: createCopy(this.state.animations)
    };

    return desc;
  }
  setStateFromProj = (proj, callback) => {
    this.setState({ 
        path: undefined,
        isDirty: false,
        image: undefined,
        frameDefaults: proj.frameDefaults,
        spritesheet: proj.spritesheet,
        animations: proj.animations,
        activeAnimationIndex: proj.modules.animations.activeIndex,
        playerZoom: proj.modules.player.zoom,
        advancedPlayerZoom: proj.modules.advancedPlayer.zoom,
        advancedPlayerPosition: proj.modules.advancedPlayer.position,
        advancedPlayerVelocity: proj.modules.advancedPlayer.velocity,
        spritesheetZoom: proj.modules.spritesheet.zoom,
        spritesheetPath: proj.modules.spritesheet.path
      },
      () => { 
        if(callback) { 
          callback();
        } 
      }
    );
  }  

  createTitle = () => {
    let name = "Ester - ";
    if(this.state.path) {
      name += path.parse(this.state.path).base;
    }

    if(this.state.isDirty) {
      name += "*";
    }

    return name;
  }
  setTitle = () => {
    remote
      .getCurrentWindow()
      .setTitle(this.createTitle());
  }
  newProj = () => {
    this.setStateFromProj(createEmptyProject());
  }    
  loadRecentProj = () => {
    const 
      path = getRecentProjectPath(),
      proj = loadRecentProject(path);

    this.setStateFromProj(proj, () => this.setState({ path: path }));
  }

  handleOnNewProjectCreated = (e) => {
    this.setStateFromProj(e.proj);
  }
  handleOnProjectOpened = (e) => {
    this.setStateFromProj(e.proj, () => {
      this.setState({ path: e.path });
      if(this.state.spritesheetPath) {
        loadSpritesheet(this.state.spritesheetPath, (img) => this.setState({ image: img }));        
      }
    });
  }    
  handleOnProjectSaved = (e) => {
    this.setState({ 
      path: e.path, 
      isDirty: false 
    });
  }

  getActiveAnimation = () => {
    return this.state.animations[this.state.activeAnimationIndex];
  }
  getAnimation = (index) => {
    return this.state.animations[index];
  }
  handleOnActiveAnimationChange = (e) => {
    const index = 
      e.newIndex < this.state.animations.length - 1
        ? e.newIndex
        : this.state.animations.length - 1;    

    this.setState({
      activeAnimationIndex: index,
      isDirty: true
    });
  }
  handleOnAnimsChange = (e) => {
    if(e.activeIndex !== undefined) {
        this.setState({
        animations: e.anims,
        activeAnimationIndex: e.activeIndex,
        isDirty: true
      });
    } else {
      this.setState({
        animations: e.anims,
        isDirty: true
      });
    }
  }  

  handleOnFramesChange = (e) => {
    const 
      anims = createCopy(this.state.animations),
      anim = anims[this.state.activeAnimationIndex];
    anim.frames = e.frames;

    this.setState({
      animations: anims,
      isDirty: true
    });
  }
  handleOnResetFramesClick = () => {
    const 
      anims = createCopy(this.state.animations),
      anim = anims[this.state.activeAnimationIndex],
      frames = anim.frames; 

    frames.forEach(f => f.duration = undefined);
    
    this.setState({
      animations: anims,
      isDirty: true
    });
  }

  handleOnSpritesheetChange = (e) => {
    this.setState({
      spritesheet: e.spritesheet,
      isDirty: true
    })
  }  
  handleOnSpritesheetZoomChange = (e) => {
    this.setState({
      spritesheetZoom: e.zoom,
      isDirty: true
    })
  }   
  handleOnSpritesheetFileChange = (e) => {
    loadSpritesheet(e.file.path, (img) => this.setState({ image: img, spritesheetPath: e.file.path }));
  }


  handleOnResetDefaultSettingsClick = () => {
    this.setState({
      frameDefaults: createFrameDefaults(),
      isDirty: true
    });
  }
  handleOnDefaultSettingsChange = (e) => {
    this.setState({
      frameDefaults: e.settings,
      isDirty: true
    });
  }

  handleOnPlayerZoomChange = (e) => {
    this.setState({ 
      playerZoom: e.zoom,
      isDirty: true
    });    
  }

  handleOnAdvancedPlayerVelocityChange = (e) => {
    this.setState({
      advancedPlayerVelocity: e.velocity,
      isDirty: true
    });
  }
  handleOnAdvancedPlayerPositionChange = (e) => {
    this.setState({
      advancedPlayerPosition: e.position,
      isDirty: true
    });
  }
  handleOnAdvancedPlayerZoomChange = (e) => {
    this.setState({
      advancedPlayerZoom: e.zoom,
      isDirty: true
    });
  }    
  handleOnAdvancedPlayerResetClick = () => {
    this.setState({
      advancedPlayerZoom: 1,
      advancedPlayerVelocity: createDefaultVelocity(),
      advancedPlayerPosition: createDefaultPosition(),
      isDirty: true
    });
  }

  componentDidMount = () => {
    this.mainMenu = new MainMenu(this);
    const file = this.mainMenu.buildFileMenu({
        onNewProjectCreated: this.handleOnNewProjectCreated,
        onProjectSaved: this.handleOnProjectSaved,
        onProjectOpened: this.handleOnProjectOpened,
        onExit: this.handleOnExit
      }
    );
    const edit = this.mainMenu.buildEditMenu();
    this.menu = new Menu()

    this.menu.append(file);
    this.menu.append(edit);
    Menu.setApplicationMenu(this.menu);      

    if(this.loadRecent){
      this.loadRecentProj();
    }
  }

  createAnimDefaults = () => {
    const defaults = {
      spriteSize: this.state.spritesheet.spriteSize,
      gridSize: this.state.spritesheet.gridSize,
      frameDuration: this.state.frameDefaults.duration,
      frameOffset: this.state.frameDefaults.offset,
      frameMargin: this.state.frameDefaults.margin
    };    

    return defaults;
  }
  renderAnimationsModule = () => {
    return (
      <AnimationsModule 
        anims={this.state.animations}
        defaults={this.createAnimDefaults()}
        activeIndex={this.state.activeAnimationIndex}
        onActiveAnimationChange={this.handleOnActiveAnimationChange}
        onAnimsChange={this.handleOnAnimsChange}
      />
    );
  }
  renderFramesModule = () => {
    const frames = 
      this.state.activeAnimationIndex !== undefined 
        ? this.getAnimation(this.state.activeAnimationIndex).frames 
        : undefined;

    return (
      <FramesModule 
        id="frames"
        className="Module"
        frames={frames}
        onFramesChange={this.handleOnFramesChange}
        onResetClick={this.handleOnResetFramesClick}
      />
    );
  }
  renderPlayerModule = () => {
    return (
      <PlayerModule 
        id="Player"
        className="Module"
        image={this.state.image}
        spritesheet={this.state.spritesheet}
        animation={this.getActiveAnimation(this.state.activeAnimationIndex)}
        zoom={this.state.playerZoom}
        frameDefaults={this.state.frameDefaults}
        onZoomChange={this.handleOnPlayerZoomChange}
      />   
    );
  }
  renderAdvancedPlayerModule = () => {
    return (
      <AdvancedPlayerModule 
        id="AdvancedPlayer"
        className="Module"
        image={this.state.image}
        spritesheet={this.state.spritesheet}
        animation={this.getActiveAnimation(this.state.activeAnimationIndex)}
        velocity={this.state.advancedPlayerVelocity}
        position={this.state.advancedPlayerPosition}
        zoom={this.state.advancedPlayerZoom}
        frameDefaults={this.state.frameDefaults}        
        onVelocityChange={this.handleOnAdvancedPlayerVelocityChange}
        onPositionChange={this.handleOnAdvancedPlayerPositionChange}
        onZoomChange={this.handleOnAdvancedPlayerZoomChange}
        onResetClick={this.handleOnAdvancedPlayerResetClick}
      />    
    );
  }
  renderSpritesheetModule = () => {
    return (
      <SpritesheetModule
        id="Spritesheet-Module"
        className="Module Module-Spritesheet"
        onFileChange={this.handleOnSpritesheetFileChange}
        spritesheet={this.state.spritesheet}
        image={this.state.image}
        imageName={this.state.spritesheetPath}
        zoom={this.state.spritesheetZoom}
        anims={this.state.animations}
        activeAnimIndex={this.state.activeAnimationIndex}
        onSpritesheetChange={this.handleOnSpritesheetChange}
        onAnimsChange={this.handleOnAnimsChange}
        onZoomChange={this.handleOnSpritesheetZoomChange}
      />
    );
  }
  renderFrameSettingsModule = () => {
    return (
      <FrameSettingsModule
        id="Defaultsettings-Module"
        className="Module"
        settings={this.state.frameDefaults}
        onSettingsChange={this.handleOnDefaultSettingsChange}
        onResetClick={this.handleOnResetDefaultSettingsClick}
        />          
    );
  }   
  toggleSaveMenuItem = () => {
    if(!this.menu) {
      return;
    }
    const menuItem = this.menu.getMenuItemById("Save");
    menuItem.enabled = this.state.path && this.state.isDirty;
  }       
  render() {
    this.setTitle();
    this.toggleSaveMenuItem();

    return (
      <div
        className="app"
      >
          <div 
            className="app-panel"
            id="Left-panel"
          >
            {this.renderAnimationsModule()}
            {this.renderFramesModule()}
          </div>
          <div 
            className="app-panel"
            id="Centre-panel"
          >
            {this.renderPlayerModule()}
            {this.renderAdvancedPlayerModule()}
          </div>                 
        <div 
          className="app-panel"
          id="Right-panel"
        >
          {this.renderSpritesheetModule()}
          {this.renderFrameSettingsModule()}
        </div>
      </div>
    );
  }  
}

export default App;