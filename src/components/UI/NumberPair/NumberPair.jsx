import React from 'react';
import { createUpdatedCopy } from '../../../services/copyService';
import { getInteger } from '../../../services/integerService';

import "./NumberPair.css"


export default class NumberPair extends React.Component {
    constructor(props){
        super(props);

        this.state = {
          pair: this.props.pair
        };

        this.handleOnValue1Change = this.handleOnValue1Change.bind(this);            
        this.handleOnValue2Change = this.handleOnValue2Change.bind(this);            
    }
    static getDerivedStateFromProps = (nextProps, prevState) => {
      return {
        pair: nextProps.pair
      };
    }

    handleOnValueChange = (name, value) => {
      const pair =
        createUpdatedCopy(
          this.state.pair, 
          name, 
          getInteger(value, this.props.mandatory));

      this.setState({ 
          pair: pair
        },
        () => 
          this.props.onPairChange({
              pair: this.state.pair
            }
          )
      );
    }
    handleOnValue1Change = (e) => {
      this.handleOnValueChange("value1", e.target.value);
    }
    handleOnValue2Change = (e) => {
      this.handleOnValueChange("value2", e.target.value);
    }    
    
    getStepSize = () => {
      return this.props.step 
        ? this.props.step 
        : 1;
    }
    renderHeading = () => {
      return (
        <h5 
          className="number-pair"
        >        
          {this.props.heading}
        </h5> 
      );
    }
    renderLabel = (label) => {
      return (
        <label 
          className="number-pair"
        >
          {label}
        </label>        
      );
    }
    renderInput = (placeholder, value, onValueChangeCallback) => {
      return (
        <input 
          className={this.props.inputClassName ? this.props.inputClassName : "number-pair"}
          type="number"
          step={this.getStepSize()}
          placeholder={placeholder}
          value={value === undefined ? "" : value}
          min={this.props.min === undefined ? null : this.props.min}
          onChange={onValueChangeCallback}
        />
      );
    }
    render() {
      return (
        <div 
          className="number-pair"
        >
          {this.renderHeading()}

          <div 
            className={"number-pair-body"}
          >
            {this.renderLabel(this.props.label1)}
            {this.renderInput(this.props.placeholder1, this.state.pair.value1, this.handleOnValue1Change)}

            {this.renderLabel(this.props.label2)}
            {this.renderInput(this.props.placeholder2, this.state.pair.value2, this.handleOnValue2Change)}
          </div>          
        </div>
      );
    }
  }