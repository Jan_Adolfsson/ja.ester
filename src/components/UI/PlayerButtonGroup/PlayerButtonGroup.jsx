import React from 'react';
import HeaderGroup from '../../UI/CollapsablePanel/Components/HeaderGroup/HeaderGroup';
import HeaderButton from '../../UI/CollapsablePanel/Components/HeaderButton/HeaderButton';
import Zoom from '../../UI/Zoom/Zoom';

export default function PlayerButtonGroup(props) {
    return (
        <HeaderGroup>    
            <Zoom
              value={props.zoom}
              min={1}
              max={10}
              step={1}
              onValueChange={props.onZoomChange}
            />                      
          <HeaderButton 
            icon="fas fa-play"
            isPressed={props.isPlayButtonPressed}
            isDisabled={props.isPlayButtonDisabled}
            isToggle={true}
            onClick={props.onPlayClick}/>
          <HeaderButton 
            icon="fas fa-pause"
            isPressed={props.isPauseButtonPressed}            
            isToggle={true}
            onClick={props.onPauseClick}/>
          <HeaderButton 
            icon="fas fa-stop"
            onClick={props.onStopClick}/>
          {props.isRepeatButtonVisible && 
            <HeaderButton 
                icon="fas fa-redo"
                isPressed={props.isRepeatButtonPressed}
                onClick={props.onRepeatClick}
                isToggle={true}/>}
        </HeaderGroup>
      );
}