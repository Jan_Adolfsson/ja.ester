import React from 'react';
import { createUpdatedCopy } from '../../../services/copyService';
import { getInteger } from '../../../services/integerService';

import "./NumberQuadrupel.css"

export default class NumberQuadrupel extends React.Component {
    constructor(props){
        super(props);

        this.state = {
          quadrupel: this.props.quadrupel
        };

        this.handleOnValue1Change = this.handleOnValue1Change.bind(this);            
        this.handleOnValue2Change = this.handleOnValue2Change.bind(this);            
        this.handleOnValue3Change = this.handleOnValue3Change.bind(this);            
        this.handleOnValue4Change = this.handleOnValue4Change.bind(this);                            
    }
    static getDerivedStateFromProps = (nextProps, prevState) => {
      return {
        quadrupel: nextProps.quadrupel
      };
    }    
    handleOnValueChange = (name, value) => {
      const quadrupel = 
        createUpdatedCopy(
          this.state.quadrupel, 
          name, 
          getInteger(value, this.props.mandatory));

      this.setState({
          quadrupel: quadrupel
        },
        () => this.props.onQuadrupelChange({ 
            quadrupel: this.state.quadrupel 
          }
        )
      );
    }
    handleOnValue1Change = (e) => {
      this.handleOnValueChange ("value1", e.target.value);
    }
    handleOnValue2Change = (e) => {
      this.handleOnValueChange ("value2", e.target.value);
    }
    handleOnValue3Change = (e) => {
      this.handleOnValueChange ("value3", e.target.value);
    }
    handleOnValue4Change = (e) => {
      this.handleOnValueChange ("value4", e.target.value);
    }            

    getStepSize = () => {
      return this.props.step ? 
        this.props.step : 
        1;
    }    
    renderHeading = () => {
      return (
        <h5 className="numberquadrupel">        
          {this.props.heading}
        </h5>         
      );
    }
    renderLabel = (label) => {
      return (
        <label 
          className="numberquadrupel"
        >
          {label}
        </label>        
      );
    }
    renderInput = (placeholder, value, onChangeCallback) => {
      return (
        <input 
          className={this.props.inputClassName}
          type="number"
          step={this.getStepSize()}
          placeholder={placeholder}
          value={value}
          min={this.props.min === undefined ? null : this.props.min}
          onChange={onChangeCallback}
        />
      );
    }
    render() {
      return (
        <div 
          className="numberquadrupel"
        >
          {this.renderHeading()}

          {this.renderLabel(this.props.label1)}
          {this.renderInput(this.props.placeholder1, this.state.quadrupel.value1, this.handleOnValue1Change)}
          
          {this.renderLabel(this.props.label2)}
          {this.renderInput(this.props.placeholder2, this.state.quadrupel.value2, this.handleOnValue2Change)}
          
          {this.renderLabel(this.props.label3)}
          {this.renderInput(this.props.placeholder3, this.state.quadrupel.value3, this.handleOnValue3Change)}
          
          {this.renderLabel(this.props.label4)}
          {this.renderInput(this.props.placeholder4, this.state.quadrupel.value4, this.handleOnValue4Change)}                              
        </div>
      );
    }
  }