import React from 'react';
import "./Zoom.css"

export default class Zoom extends React.Component {
  constructor(props){
      super(props);

      this.state= {
        value: this.props.value
      };
      
      this.handleOnChange = this.handleOnChange.bind(this);    
  }

    handleOnChange = (e) => {
      this.setState({
          value: e.target.value
        },
        () => this.props.onValueChange({
            value: this.state.value
          }
        )
      );
    }

    render() {
      return (
        <div 
          className="zoom-container">
            <input 
              className="zoom"
              type="range" 
              min={this.props.min}
              max={this.props.max}
              value={this.state.value}
              step={this.props.step}
              onChange={this.handleOnChange}
            />
        </div>
        )
    }
  }