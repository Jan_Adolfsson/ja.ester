import React from 'react';
import Header from './Components/Header/Header';
import Body from './Components/Body/Body';
import "./CollapsablePanel.css"

export default class CollapsablePanel extends React.Component {
  constructor(props){
      super(props);
  
      this.state = {
        isCollapsed: false,
        isCollapsable: this.props.isCollapsable
      };

      this.handleOnToggleButtonClick = this.handleOnToggleButtonClick.bind(this);  
    }

    handleOnToggleButtonClick = () => {
      this.setState({
          isCollapsed: !this.state.isCollapsed
        });
    }

    render() {
      return (
        <div 
          className="panel"
        >
          <Header 
            onToggleButtonClick={this.handleOnToggleButtonClick}
            text={this.props.header}
            isCollapsed={this.state.isCollapsed}
            isActive={this.props.isActive}
            showToggleButton={this.props.isCollapsable}
          >
            {this.props.buttons}
          </Header>

          {!this.state.isCollapsed && 
            <Body>
              {this.props.children}                
            </Body>
          }
        </div>
      );
    }
  }