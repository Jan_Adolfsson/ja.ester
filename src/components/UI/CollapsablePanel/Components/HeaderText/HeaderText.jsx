import React from 'react';
import "./HeaderText.css"

export default function HeaderText(props) {
  return (
    <h3 
      className={props.isActive ? "header-text-active" : "header-text"}
    >
      {props.text}      
    </h3>      
  );
}