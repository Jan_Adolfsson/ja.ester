import React from 'react';
import HeaderButton from '../HeaderButton/HeaderButton'
import HeaderText from '../HeaderText/HeaderText'

import "./Header.css"

export default class Header extends React.Component {
  constructor(props){
      super(props);

      this.state = {
        isCollapsed: this.props.isCollapsed
      };
      
      this.handleOnClick = this.handleOnClick.bind(this);
  }

  handleOnClick = () => {
    this.setState({
        isCollapsed: !this.state.isCollapsed
      },
      () => {
        this.props.onToggleButtonClick();
      }
    );
  }
  render() {
    return (
      <div 
        className={this.props.isActive ? "panel-header-active" : "panel-header"}
      >
        <HeaderText
          text={this.props.text}
          isActive={this.props.isActive}
        />
        {this.props.children}
        <HeaderButton 
          icon={this.state.isCollapsed ? "fas fa-angle-down" : "fas fa-angle-up"}
          onClick={this.handleOnClick}
      />        
      </div>
    );
  }
}