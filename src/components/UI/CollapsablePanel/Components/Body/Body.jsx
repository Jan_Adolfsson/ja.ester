import React from 'react';
import "./Body.css"

export default class Body extends React.Component {
  render() {
    return (
      <div 
        className="panel-body"
      >
        {this.props.children}
      </div>
    );
  }
}