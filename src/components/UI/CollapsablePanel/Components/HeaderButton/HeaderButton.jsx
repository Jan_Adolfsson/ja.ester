import React from 'react';
import "./HeaderButton.css"

export default function HeaderButton(props) {
  function selectDivClass(isDisabled, isPressed) {
    const className="header-button";

    if(isDisabled) {
      return `${className} disabled`;
    } else if(isPressed) {
      return `${className} down`;
    } else {
      return `${className}`;
    }
  }

  return (
    <div 
      className={selectDivClass(props.isDisabled, props.isPressed)}
      onClick={props.isDisabled ? (null) : props.onClick}
    >
      <i
        className={"header-button-i " + props.icon}
      />
    </div>
  );
}