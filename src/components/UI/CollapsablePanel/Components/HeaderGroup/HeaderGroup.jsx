import React from 'react';
import "./HeaderGroup.css"

export default function HeaderGroup(props) {
  return (
    <div 
      className="header-group"
    >
      {props.children}
    </div>
  );
}  