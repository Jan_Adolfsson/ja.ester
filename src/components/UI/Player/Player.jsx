import React from 'react';
import "./Player.css";

export default class Player extends React.Component {

  constructor(props){
    super(props);

    this.divRef = React.createRef();
    this.frameIndex = 0;
    this.img = undefined;
    this.timer = undefined;

    this.state = {
      action: this.props.action,
      position: this.props.position
    };

    this.resetPos();
  }
  static getDerivedStateFromProps = (nextProps, prevState) => {
    return {
      position: nextProps.position
    };
  }   
  resetPos = () => {
    if(!this.state.position) {
      this.pos = {
        x: 0,
        y: 0
      };
    } else {
      this.pos = {
        x: this.state.position.x,
        y: this.state.position.y
      };
    }
  }
  getIndex = () => {
    var 
      x = this.props.animation.index.x,
      y = this.props.animation.index.y;

    x += this.frameIndex;      

    while(x >= this.props.gridSize.cols) {
      ++y;
      x -= this.props.gridSize.cols;
    } 
    
    return {
      x: x,
      y: y
    };
  }
  indexToPos = (index) => {
    return {
      x: index.x * this.props.spriteSize.width,
      y: index.y * this.props.spriteSize.height
    }
  }
  getSourcePos = () => {
    return this.indexToPos(this.getIndex());
  }
  getScaledSpriteSize = () => {
    const size = {
      width: this.props.spriteSize.width * this.props.zoom,
      height: this.props.spriteSize.height * this.props.zoom
    };
    return size;
  }
  secondIfUndefined = (value1, value2) => {
    const value = value1 === undefined ? value2 : value1;
    return value;
  }
  getOffset = () => {
    return {
      x: this.secondIfUndefined(this.props.animation.frameOffset.x, this.props.frameDefaults.offset.x),
      y: this.secondIfUndefined(this.props.animation.frameOffset.y, this.props.frameDefaults.offset.y)
    };
  }
  offsetPos = (pos, offset) => {
    const offsetPos = {
      x: pos.x + offset.x,
      y: pos.y + offset.y
    };
    return offsetPos;
  }
  draw = () => {
    this.drawImage();
  }
  drawImage = () => {
    if(!this.props.frameDefaults) {return;}
    const sourcePos = this.getSourcePos();
    const offset = this.getOffset();
    const offsetPos = this.offsetPos(sourcePos, offset);
    const ctx = this.refs.canvas.getContext("2d");
    const scaledSize = this.getScaledSpriteSize();

    ctx.drawImage(
      this.props.spritesheet,
      offsetPos.x,
      offsetPos.y,
      this.props.spriteSize.width,
      this.props.spriteSize.height,
      this.pos.x,
      this.pos.y,
      scaledSize.width,
      scaledSize.height);         
  }

  clearFrame = () => {
    const ctx = this.refs.canvas.getContext("2d");
    ctx.clearRect(
      0,
      0,
      this.refs.canvas.width,
      this.refs.canvas.height);
  }

  updatePos = (pos, velocity) => {
    const newPos = {
      x: pos.x + velocity.x,
      y: pos.y + velocity.y
    };
    return newPos;
  }
  updatePositionMovingUp = (divSize, scaledSize, pos, velocity) => {
    let newPos = this.updatePos(pos, velocity);
    if(newPos.y + scaledSize.height <= 0) {
      newPos.y = divSize.height;
    }    
    return newPos;
  }
  updatePositionMovingUpAndRight = (divSize, scaledSize, pos, velocity) => {
    let newPos = this.updatePos(pos, velocity);
    if(newPos.x >= divSize.width) {
      newPos.x = -scaledSize.width;
    }
    if(newPos.y + scaledSize.height >=0) {
      newPos.y = divSize.height;
    }    
    return newPos;
  }
  updatePositionMovingRight = (divSize, scaledSize, pos, velocity) => {
    let newPos = this.updatePos(pos, velocity);
    if(newPos.x >= divSize.width) {
      newPos.x = -scaledSize.width;
    }    
    return newPos;
  }
  updatePositionMovingDownAndRight = (divSize, scaledSize, pos, velocity) => {
    let newPos = this.updatePos(pos, velocity);
    if(newPos.x >= divSize.width) {
      newPos.x = -scaledSize.width;
    }    
    if(newPos.y >= divSize.height) {
      newPos.y = -scaledSize.height;
    }    
    return newPos;
  }
  updatePositionMovingDown = (divSize, scaledSize, pos, velocity) => {
    let newPos = this.updatePos(pos, velocity);
    if(newPos.y >= divSize.height) {
      newPos.y = -scaledSize.height;
    }    
    return newPos;
  }
  updatePositionMovingDownAndLeft = (divSize, scaledSize, pos, velocity) => {
    let newPos = this.updatePos(pos, velocity);
    if(newPos.x + scaledSize.width <= 0) {
      newPos.x = divSize.width;
    }    
    if(newPos.y >= divSize.height) {
      newPos.y = -scaledSize.height;
    }    
    return newPos;
  }
  updatePositionMovingLeft = (divSize, scaledSize, pos, velocity) => {
    let newPos = this.updatePos(pos, velocity);
    if(newPos.x + scaledSize.width <= 0) {
      newPos.x = divSize.width;
    }    
    return newPos;
  }
  updatePositionMovingUpAndLeft = (divSize, scaledSize, pos, velocity) => {
    let newPos = this.updatePos(pos, velocity);
    if(newPos.x + scaledSize.width <= 0) {
      newPos.x = divSize.width;
    }    
    if(newPos.y + scaledSize.height <=0) {
      newPos.y = divSize.height;
    }    
    return newPos;
  }
  getScaledDivSize = () => {
    return {
      width: this.divSize.width * this.props.zoom,
      height: this.divSize.height * this.props.zoom
    }
  }
  updatePosition = () => {
    const divSize = this.getScaledDivSize();
    const scaledSize = this.getScaledSpriteSize();
    if(this.props.velocity.x === 0 &&
      this.props.velocity.y < 0) {
        this.pos = this.updatePositionMovingUp(divSize, scaledSize, this.pos, this.props.velocity);
    } else if(this.props.velocity.x > 0 &&
      this.props.velocity.y < 0) {
        this.pos = this.updatePositionMovingUpAndRight(divSize, scaledSize, this.pos, this.props.velocity);
    } else if(this.props.velocity.x > 0 &&
      this.props.velocity.y === 0) {
        this.pos = this.updatePositionMovingRight(divSize, scaledSize, this.pos, this.props.velocity);
    } else if(this.props.velocity.x > 0 &&
      this.props.velocity.y > 0) {
        this.pos = this.updatePositionMovingDownAndRight(divSize, scaledSize, this.pos, this.props.velocity);
    } else if(this.props.velocity.x === 0 &&
      this.props.velocity.y > 0) {
        this.pos = this.updatePositionMovingDown(divSize, scaledSize, this.pos, this.props.velocity);
    } else if(this.props.velocity.x < 0 &&
      this.props.velocity.y > 0) {
        this.pos = this.updatePositionMovingDownAndLeft(divSize, scaledSize, this.pos, this.props.velocity);
    } else if(this.props.velocity.x < 0 &&
      this.props.velocity.y === 0) {
        this.pos = this.updatePositionMovingLeft(divSize, scaledSize, this.pos, this.props.velocity);
    } if(this.props.velocity.x < 0 &&
      this.props.velocity.y < 0) {
        this.pos = this.updatePositionMovingUpAndLeft(divSize, scaledSize, this.pos, this.props.velocity);
    }
  }

  isMoving = () => {
      if(!this.props.velocity) {
      return false;
    }
    return this.props.velocity.x ||
      this.props.velocity.y;
  }
  updateFrameIndex = () => {
    ++this.frameIndex;
    if(this.frameIndex >= this.props.frameCount) {
      this.frameIndex = 0;
      if(!this.props.repeat) {
        this.stopAnimation();
      }
    }   
  }
  isStopped = () => {
    return this.action === "stopped";
  }
  updateFrame = () => {
    if(!this.props.spritesheet) {
      return;
    }
    
    this.clearFrame();
    this.updateFrameIndex();

    if(this.isMoving()) {
      this.updatePosition();
    }
    this.draw();

    if(!this.isStopped()) {
      this.timer = setTimeout(this.updateFrame, this.getInterval())
    }
  }

  getDurationFromAnimOrDefault = () => {
    return this.props.animation.frameDuration ? 
      this.props.animation.frameDuration : 
      this.props.frameDefaults.duration;
  }
  getCurrentFrame = () => {
    return this.props.animation.frames[this.frameIndex];
  }
  isFramesEmpty = () => {
    return this.props.animation.frames.length === 0;
  }
  getDurationFromFrameOrAnim = () => {
    if(this.isFramesEmpty()) {
      return this.getDurationFromAnimOrDefault();
    }
    const frame = this.getCurrentFrame();
    return frame.duration ? 
      frame.duration : 
      this.getDurationFromAnimOrDefault();
  }
  getInterval = () => {
    return this.getDurationFromFrameOrAnim();
  }
  startAnimation = () => {
    this.timer = setTimeout(this.updateFrame, this.getInterval());     
    this.action = "running";
  }

  pauseAnimation = () => {
    this.action = "paused";
    clearTimeout(this.timer);     
  } 
  resetIndex = () => {
    this.frameIndex = 0;
  }
  stopAnimation = () => {
    this.resetIndex();
    clearTimeout(this.timer);     
    this.action = "stopped";
    this.props.onAnimationStop();
  }   

  isMandatoryPropertiesSet = () => {
    if(this.props.spritesheet &&
      this.props.spriteSize &&
      this.props.gridSize &&
      this.props.animation &&
      this.props.animation.frames) {
      return true;
    }

    return false;
  }
  componentDidMount() {
    this.updateDivSize();
    if(!this.isMandatoryPropertiesSet()) {
      return;
    }
    this.draw();
  }
  updateDivSize = ()  => {
    const size = this.getDivSize();
    this.divSize = size;
  }
  componentDidUpdate() {
    this.updateDivSize();
    if(!this.isMandatoryPropertiesSet()) {
      return;
    }

    if(this.props.action === "play" && this.action !== "running" && this.props.frameCount > 0) {
      this.startAnimation();
    } else if(this.props.action === "stop" && this.action !== "stopped") {
      this.stopAnimation();
      this.resetPos();
    } else if(this.props.action === "pause") {
      this.pauseAnimation();
    }    
    this.draw();
  }
  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  getDivSize = () => {
    const size = {
      width: this.divRef.current.clientWidth,
      height: this.divRef.current.clientHeight
    };
    return size;
  }
  setCanvasSize = () => {
    if(this.props.spritesheet && this.divSize) {
      this.refs.canvas.width = this.divSize.width * this.props.zoom;
      this.refs.canvas.height = this.divSize.height * this.props.zoom;
    }    
  }

  render() {
    this.setCanvasSize();

    return (
      <div
        ref={this.divRef}
        className="player"
        style={this.props.style && this.props.style}
      >
        <canvas
          ref="canvas"
          className="player">
        </canvas>
      </div>
    );
  }
}