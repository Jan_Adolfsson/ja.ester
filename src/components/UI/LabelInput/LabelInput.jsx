import React from 'react';
import "./LabelInput.css";
import { getInteger } from '../../../services/integerService';

export default class LabelInput extends React.Component {
  constructor(props){
      super(props);

      this.inputRef = React.createRef();

      this.state = {
        value: this.props.value
      };

      this.handleOnValueChange = this.handleOnValueChange.bind(this);            
  }
  static getDerivedStateFromProps = (nextProps, prevState) => {
    return {
      value: nextProps.value
    };
  }
  handleOnTextValueChange = (e) => {
    this.setState({ 
        value: e.target.value
      },
      () => {
        this.props.onValueChange({ 
            value: this.state.value
          });
      }
    );
  }
  handleOnNumberValueChange = (e) => {
    this.setState({ 
        value: getInteger(e.target.value, this.props.mandatory)
      },
      () => {
        this.props.onValueChange({ 
            value: this.state.value
        });
      }
    );
  }
  handleOnValueChange = (e) => {
    this.getType() === "text"
      ? this.handleOnTextValueChange(e)
      : this.handleOnNumberValueChange(e);
  }
  getType = () => {
    return this.props.type ? 
      this.props.type : 
      "text";
  }
  getStepSize = () => {
    return this.props.step ? 
      this.props.step : 
      0;
  }

  render() {
    const labelClassName = 
    this.props.labelClassName 
      ? this.props.labelClassName
      : this.props.isDisabled
        ? "label-input-disabled"
        : "label-input";


    return (
      <div 
        className="label-input"
        onFocus={this.props.onFocus ? this.props.onFocus(this) : (null)}
      >
        <label 
          className={labelClassName}
        >
          {this.props.label}
        </label>

        <input 
          ref={this.inputRef}
          className={this.props.inputClassName + " label-input"}
          type={this.getType()}
          step={this.getStepSize()}
          value={this.state.value}
          min={this.props.min === undefined ? null : this.props.min}
          placeholder={this.props.placeholder}
          disabled={this.props.isDisabled}
          onChange={this.handleOnValueChange}
        />
      </div>
    );
  }
}