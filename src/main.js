const path = require('path');
const electron = require('electron');
const {ipcMain} = require('electron');

// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 1280, 
        height: 960,
        icon: path.join(__dirname, './icons/ester_icon.ico')
    });

    // and load the index.html of the app.
    const startUrl = process.env.ELECTRON_START_URL || url.format({
        pathname: path.join(__dirname, '/../build/index.html'),
        protocol: 'file:',
        slashes: true
    });
    mainWindow.loadURL(startUrl);

    // Open the DevTools.
    //mainWindow.webContents.openDevTools();

    mainWindow.on('close', (e) => {
        if(mainWindow) {
            e.preventDefault();
            mainWindow.webContents.send('app-close');
        }
    })
}

app.on('ready', createWindow);

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

ipcMain.on('closed', (e) => {
    mainWindow = null;
    app.quit();
});