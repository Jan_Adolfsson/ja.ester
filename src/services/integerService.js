export function getInteger(value, mandatory) {
  let intValue;
  if(mandatory) {
    if(value === "" || value === undefined) {
      intValue = 0;
    } else {
      intValue = parseInt(value, 10);
    }
  } else {
    if(value !== "" && !isNaN(value)) {
      intValue = parseInt(value, 10);
    }
  }
  return intValue;
}
