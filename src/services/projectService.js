const fs = window.require('fs');

export function createGridSizeDefault() {
  const size = {
    width: 8,
    height: 8
  };
  return size;
}
export function createSpriteSizeDefault() {
  const size = {
    width: 64,
    height: 64
  };
  return size;
}
export function createSpritesheet() {
  const defaults = {
    image: undefined,
    gridSize: createGridSizeDefault(),
    spriteSize: createSpriteSizeDefault()
  };

  return defaults;
}
export function createFrameDefaults() {
  const defaults = {
    margin: {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    },
    offset: {
      x: 0,
      y: 0
    },
    duration: 200
  };
  return defaults;
}
export function createAnimDefault() {
  const defaults = createFrameDefaults();
  const anim = {
    frameCount: 0,
    frameDuration: defaults.duration,
    frameMargin: defaults.margin,
    frameOffset: defaults.offset,
    frames: [],
    id: 0,
    index: {
      x: 0,
      y: 0
    },
    loop: true,
    name: "New anim"
  }
  return anim;
}
export function createDefaultVelocity() {
  const velocity = {
    x: 0,
    y: 0
  };
  return velocity;
}
export function createDefaultPosition() {
  const position = {
    x: 10,
    y: 10
  };
  return position;
}
export function createAdvancedPlayerDefault() {
  const player = {
    zoom: 1,
    velocity: createDefaultVelocity(),
    position: createDefaultPosition()
  };
  return player;
}
export function createAnimationsDefault() {
  const animations = {
    activeIndex: 0
  };
  return animations;
}
export function createPlayerDefault() {
  const player = {
    zoom: 1,
    repeat: false
  };
  return player;
}
export function createSpritesheetDefault() {
  const spritesheet = {
    zoom: 0.25,
  };
  return spritesheet;
}
export function createEmptyProject() {
  const proj = {
    spritesheet: createSpritesheet(),
    frameDefaults: createFrameDefaults(),
    animations: [
      createAnimDefault()
    ],
    modules: {
      advancedPlayer: createAdvancedPlayerDefault(),
      animations: createAnimationsDefault(),
      defaults: {
      },
      player: createPlayerDefault(),
      spritesheet: createSpritesheetDefault(),
    },
    loadRecent: false
  };

  return proj;
}
export function createProjectFromState(state) {
  const proj = {
    spritesheet: state.spritesheet,
    frameDefaults: state.frameDefaults,
    animations: state.animations,
    modules: {
      advancedPlayer: {
        zoom: state.advancedPlayerZoom,
        velocity: state.advancedPlayerVelocity,
        position: state.advancedPlayerPosition
      },
      animations: {
        activeIndex: state.activeAnimationIndex
      },
      defaults: {

      },
      player: {
        zoom: state.playerZoom
      },
      spritesheet: {
        zoom: state.spritesheetZoom,
        path: state.spritesheetPath
      }
    },
    loadRecent: false
  }
  return proj;
}
export function loadRecentProject() {

}
function jsonToProj(json) {
  return JSON.parse(json);
}
export function loadProject(path, callback) {
  fs.readFile(path, "utf8", (err, data) => {
    if (err) {
      callback(err, undefined);
      return;
    };

    const proj = jsonToProj(data);
    callback(proj, undefined);
  });
}
function projToJson(proj) {
  return JSON.stringify(proj, undefined, "    ");
}
export function saveProject(proj, fileName, callback) {
  const json = projToJson(proj);
  fs.writeFile(fileName, json, callback);
}
export function getRecentProjectPath() {
  return undefined;
}
