const {clipboard} = window.require('electron');

export function copyAsJsonToClipboard(obj, name) {
  const json = JSON.stringify(obj, null, "    ");
  clipboard.writeText(json, name);    
}