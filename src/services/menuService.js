import { 
  showSaveQuestion,
  showSaveDlg,
  showOpenDlg,
  showErrorDlg
  } from './dialogService';
import { 
  createEmptyProject, 
  loadProject, 
  saveProject
} from './projectService';
import { exportSpritesheetDesc } from './exportService';

const {remote} = window.require('electron');
const {MenuItem} = remote;

export function createMenu(label, items) {
  const menu = new MenuItem ({
    label: label,
    type: 'submenu',
    submenu: items
  }); 
  
  return menu;
}

export function createTextMenuItem(label, role, clickCallback){
  const item = new MenuItem({
    id: label,
    label: label,
    role: role,
    click() {
        clickCallback();
    }
  });    
  
  return item;
}

export function createSeparatorMenuItem(){
  const item = new MenuItem({
    type: "separator"
  });    

  return item;
}

export class MainMenu {
  constructor(app) {
    this.app = app;
  }

  saveProj = (path) => {
    const proj = this.app.getProj();
    saveProject(
      proj, 
      path, 
      (err) => { 
        if(!err) { 
          this.fileMenuCallbacks.onProjectSaved({ 
            proj: proj, 
            path: path 
          });
        } else {
          showErrorDlg("Error saving file.");
        }
      }
    );    
  }  
  
  saveAsAndProceed() {
    const reply = showSaveDlg();
    if(!reply) {
      return false;
    }

    this.saveProj(reply);    
    return true;
  }    
  saveAndProceed() {
    if(!this.app.isProjDirty()){
      return true;
    }

    const reply = showSaveQuestion();
    if(reply === "cancel") {
      return false;
    } 
    
    if(reply === "save") {
      if(this.app.isProjNew()) {
        if(!this.saveAsAndProceed()) {
          return false;
        }
      } else {
        this.saveProj(this.app.getProjPath());
      }
    } 

    return true;
  }      
  onMenuItemSaveClick = () => {
    this.saveAndProceed();
  }
  onMenuItemSaveAsClick = () => {
    this.saveAsAndProceed();
  }

  export = (path, desc) => {
    exportSpritesheetDesc(
      desc, 
      path, 
      (err) => { 
        if(!err) { 
        } else {
          showErrorDlg("Error exporting.");
        }
      }
    );    
  }  
  onMenuItemExportClick = () => {
    const reply = showSaveDlg();
    if(reply) {
      const desc = this.app.getSpritesheetDesc();      
      this.export(reply, desc);   
    }
  }

  onMenuItemNewClick = () => {
    if(!this.saveAndProceed()) {
      return;
    }        

    const proj = createEmptyProject();
    this.fileMenuCallbacks.onNewProjectCreated({
      proj: proj,
    });
  }
  onMenuItemOpenClick = (e) => {
    if(!this.saveAndProceed()) {
      return;
    }

    const reply = showOpenDlg();
    if(!reply) {
      return;
    }

    loadProject(reply[0], (proj, err) => {
      if(err) {
        showErrorDlg("Loading file failed.");
        return;
      }
      this.fileMenuCallbacks.onProjectOpened({ 
        proj: proj,
        path: reply[0]
      });
    });
  }
  buildFileMenu = (callbacks) => {
    this.fileMenuCallbacks = callbacks;
    const items = [
      createTextMenuItem("New", undefined, this.onMenuItemNewClick),
      createSeparatorMenuItem(),
      createTextMenuItem("Open...", undefined, this.onMenuItemOpenClick),
      createSeparatorMenuItem(),
      createTextMenuItem("Save", undefined, this.onMenuItemSaveClick),    
      createTextMenuItem("Save As...", undefined, this.onMenuItemSaveAsClick),          
      createTextMenuItem("Export...", undefined, this.onMenuItemExportClick),                
      createSeparatorMenuItem(),
      createTextMenuItem("Exit", "close", this.onMenuItemExitClick)
    ];

    items[4].enabled = false;
    const menu = createMenu("File", items);
    return menu;
  }    
  
  buildEditMenu = () => {
    const items = [
      createTextMenuItem("Undo", "undo"),
      createTextMenuItem("Redo", "redo"),
      createSeparatorMenuItem(),
      createTextMenuItem("Cut", "cut"),    
      createTextMenuItem("Copy", "copy"),    
      createTextMenuItem("Paste", "paste")
    ]

    const menu = createMenu("Edit", items);
    return menu;
  }      

  buildHelpMenu = () => {
    const items = [
      createTextMenuItem("About", "about"),
    ]

    const menu = createMenu("Help", items);
    return menu;
  } 
}