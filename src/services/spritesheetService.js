const fs = window.require('fs');

export function loadSpritesheet(path, callback) {
  fs.readFile(path, function (err, data) {
    const blob = new Blob([data], {type:'image/png'});
    var url = URL.createObjectURL(blob);
    var img = new Image();

    img.onload = () => callback(img);
    img.src = url;      
  });
}