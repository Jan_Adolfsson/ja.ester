export function numberPairToPoint(pair) {
  return {
    x: pair.value1,
    y: pair.value2
  };
}

export function pointToNumberPair(point) {
  return {
    value1: point.x,
    value2: point.y
  };
}

export function numberPairToSize(pair) {
  return {
    width: pair.value1,
    height: pair.value2
  };
}

export function sizeToNumberPair(size) {
  return {
    value1: size.width,
    value2: size.height
  };
}

export function numberQuadrupelToMargin(quadrupel) {
  return {
    top: quadrupel.value1,
    right: quadrupel.value2,
    bottom: quadrupel.value3,
    left: quadrupel.value4,
  }
}

export function marginToNumberQuadrupel(margin) {
  return {
    value1: margin.top,
    value2: margin.right,
    value3: margin.bottom,
    value4: margin.left,
  }
}