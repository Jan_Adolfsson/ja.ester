export function createFrame(index) {
  return {
      index: index,
      duration: undefined
  };
}

export function appendNewFrames(frames, count) {
  const index = frames.length;
  for(var i = 0; i < count; ++i) {
    const frame = createFrame(index + i);
    frames.push(frame);
  }
}

export function removeFrames(frames, count) {
  for(var i = 0; i < count; ++i) {
    frames.pop();
  }
}

export function adjustFrames(frames, frameCount) {
  if(frameCount > frames.length){
    appendNewFrames(frames, frameCount - frames.length);
  } else {
    removeFrames(frames, frames.length - frameCount);
  }
}