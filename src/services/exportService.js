const fs = window.require('fs');

export function exportSpritesheetDesc(desc, path, callback) {
    const json = JSON.stringify(desc, undefined, "    ");
    fs.writeFile(path, json, callback);
}