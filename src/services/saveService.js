import {
  showSaveQuestion,
  showSaveDlg,
} from './dialogService';
import { saveProject } from './projectService';

saveProj = (name) => {
  saveProject(
    this.app.getProj(),
    name,
    (err) => {
      if (!err) {
        const proj = this.app.getProj();
        proj.name = name;
        this.fileMenuCallbacks.onProjectSaved({
          proj: proj,
          name: name
        });
      } else {
        showErrorDlg("Error saving file.");
      }
    }
  );
}

export function saveAsAndProceed() {
  const reply = showSaveDlg();
  if (!reply) {
    return false;
  }

  this.saveProj(reply);
  return true;
}
export function saveAndProceed() {
  if (!this.app.isProjDirty()) {
    return true;
  }

  const reply = showSaveQuestion();
  if (reply === "cancel") {
    return false;
  }

  if (reply === "save") {
    if (this.app.isProjNew()) {
      if (!this.saveAsAndProceed()) {
        return false;
      }
    } else {
      this.saveProj(this.app.getProj().name);
    }
  }

  return true;
} 