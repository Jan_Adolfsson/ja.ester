export function createCopy(obj) {
  return JSON.parse(JSON.stringify(obj))
}

export function createUpdatedCopy(obj, name, value) {
  const copy = createCopy(obj);
  copy[name] = value;
  return copy;
}    
