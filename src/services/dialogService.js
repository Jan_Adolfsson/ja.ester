const {remote} = window.require('electron');
const {dialog} = remote;

export function showSaveQuestion() {
  const results = ["save", "dontsave", "cancel"];
  
  const buttonIndex = dialog.showMessageBox({ 
      type: "warning",
      title: "Ester",
      message: "Do you want to save the changes you have made?",
      detail: "Your changes will be lost if you don't save them.",
      noLink: true,
      buttons: ["Save", "Don't save", "Cancel"]
    }    
  );

  const result = results[buttonIndex];
  return result;
}

export function showErrorDlg(msg) {
    const result = dialog.showErrorBox("Ester", msg);
    return result;
}

export function showSaveDlg() {
  const result = dialog.showSaveDialog({
      title: "Save As",
      filters: [{ 
          name: "ester",
          extensions: ["json"]
        }
      ],
    }
  );

  return result;
}

export function showOpenDlg() {
  const result = dialog.showOpenDialog({
      title: "Open",
      filters: [{ 
          name: "ester",
          extensions: ["json"]
        }
      ],
    }
  );

  return result;
}