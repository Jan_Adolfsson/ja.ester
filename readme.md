# Ester (Eraserhead animation editor)

Ester is a tool for tuning spritesheet animations and generating json-files describing those animations.


### Install

At this point Ester is only available as a source code repo, so if you want to use it you have to build it yourself. But don't despair, it should be quite easy.

Ester is build upon Electron, which uses node.js, so first of all download and install node.js

Clone the repo, either through BitBucket or through the command prompt. The second option assumes you have git installed.  

	git clone https://Jan_Adolfsson@bitbucket.org/Jan_Adolfsson/Ja.Ester.git

Install required packages.  

	cd Ja.Ester
	npm install

This will take a while, but when the operation is done, you can start the application.  

	npm run

Ester should now start.


### Documentation

User manual can be found [here](https://bitbucket.org/Jan_Adolfsson/ja.ester/src/master/doc/howto.md).  


### Bugs

Current version is only a beta, so there will be bugs. When you find one, please report it [here](https://bitbucket.org/Jan_Adolfsson/ja.ester/issues?status=new&status=open).